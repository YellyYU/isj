package com.istack;

import com.istack.cloud.*;
import com.istack.cloud.identity.*;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.web.client.RestTemplate;

public class Connection {

	public Cloud cloud;

	private RestTemplate restTemplate = null;
	private HttpEntity<?> request = null;

	//tokens
	public Connection(String apiURL, String username, String password, String project) {
		
		cloud = new Cloud(apiURL, username, password, project);
		
	}
	
		
	public static Connection getConnection(String url, String username, String password, String project) {

		return new Connection(url, username, password, project);
	}

}
