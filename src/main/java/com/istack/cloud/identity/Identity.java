package com.istack.cloud.identity;

import com.istack.cloud.base.*;
import com.istack.cloud.identity.api.*;

public class Identity {

	/*private String apiURL;
	private RestTemplate httpConnection;
	private HttpEntity<?> request;*/
	
	private String apiURL;
	
	private final TokensResource TOKENS;
	private final TenantsResource TENANTS;
	
	public Identity(String apiURL) {
		this.apiURL = apiURL+"/v2.0";
		TOKENS	= new TokensResource(this.apiURL+"/tokens");
		TENANTS	= new TenantsResource(this.apiURL+"/tenants");
	}
	
	public TokensResource 	tokens() 	{ return TOKENS;	}
	public TenantsResource	tenants() 	{ return TENANTS;	}
	
	/*public String postTokens(String username, String password) {
		httpConnection = new RestTemplate();
		//PasswordCredentials pc = new PasswordCredentials(username, password);
		//Auth auth = new Auth(username, pc);
		Login login = new Login(username, password, username);

		return httpConnection.postForObject(apiURL+"/v2.0/tokens", login, String.class);
	}
	
	public Auth auth(Login login) {
		return new Auth(login);
	}
	
	public class Auth extends OpenStackRequest<Login, Access> {
		public Auth(Login login) {
			super(apiURL+"/v2.0/tokens", HttpMethod.POST, "", login, Access.class);
		}
		
	}
	
	public Versions getVersions() {
		httpConnection = new RestTemplate();
		Versions v = httpConnection.exchange(apiURL+"/", HttpMethod.GET, request, Versions.class).getBody();
		return v;
	}
	
	public VersionInfo getVersionInfo() {
		httpConnection = new RestTemplate();
		VersionInfo v = httpConnection.exchange(apiURL+"/v2.0/", HttpMethod.GET, request, VersionInfo.class).getBody();
		return v;	
	}*/
	
	
}
