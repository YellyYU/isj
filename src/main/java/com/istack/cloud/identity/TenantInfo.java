package com.istack.cloud.identity;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.istack.cloud.identity.common.Tenant;

public class TenantInfo {
	private Tenant tenant;
	
	@JsonCreator
	public TenantInfo(@JsonProperty("tenant") Tenant tenant) {
		this.tenant = tenant;
	}
	
	public Tenant getTenant() {
		return tenant;
	}

	@Override
	public String toString() {
		return "TenantInfo [tenant=" + tenant.toString() + "]";
	}
	
}
