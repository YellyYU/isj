package com.istack.cloud.identity;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Login {
	
	private class Auth{
	
		@JsonProperty("tenantName")
		private String tenantName;
		
		@JsonProperty("passwordCredentials")
		private PasswordCredentials passwordCredentials;
		
		//@JsonCreator
		Auth(String userName, String password, String tenantName){
			
			this.tenantName = tenantName;
			passwordCredentials = new PasswordCredentials(userName, password);
		}
		
		String getTenantName() {
			return tenantName;
		}
		
		PasswordCredentials getPasswordCredentials() {
			return passwordCredentials;
		}
	}
	
	@JsonProperty("auth")
	private Auth auth;
	
	public Login(String userName, String password, String tenantName) {
		auth = new Auth(userName, password, tenantName);
	}
	
	public Auth getAuth() {
		return auth;
	}
}
