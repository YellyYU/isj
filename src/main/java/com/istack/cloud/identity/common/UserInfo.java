package com.istack.cloud.identity.common;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class UserInfo {
	private String username;
	private String id;
	private String name;
	private boolean enabled;
	private String email;
	private RolesInfo rolesInfo;
	
	public UserInfo( 
			String username,
			String id, 
			String name,
			boolean enabled,
			String email,
			RolesInfo rolesInfo
			)
	{
		this.username = username;
		this.id = id;
		this.name = name;
		this.enabled = enabled;
        this.email = email;
        this.rolesInfo = rolesInfo;
	}
	
	/**
	 * @return the rolesInfo
	 */
	public RolesInfo getRolesInfo() {
		return rolesInfo;
	}

	/**
	 * @param rolesInfo the rolesInfo to set
	 */
	public void setRolesInfo(RolesInfo rolesInfo) {
		this.rolesInfo = rolesInfo;
	}

	public UserInfo(){
		
	}

	/**
	 * @return the enabled
	 */
	public boolean isEnabled() {
		return enabled;
	}

	/**
	 * @param enabled the enabled to set
	 */
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "UserInfo [" + (username != null ? "username=" + username + ", " : "")
				+ (id != null ? "id=" + id + ", " : "") + (name != null ? "name=" + name + ", " : "") + "enabled="
				+ enabled + ", " + (email != null ? "email=" + email + ", " : "")
				+ (rolesInfo != null ? "rolesInfo=" + rolesInfo : "") + "]";
	}

	
	
}