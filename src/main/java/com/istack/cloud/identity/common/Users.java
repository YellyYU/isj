package com.istack.cloud.identity.common;

import java.util.Iterator;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Users implements Iterable<UserInfo>{
	private List<UserInfo> users;
	
	@JsonCreator
    public Users(
    		@JsonProperty("users") List<UserInfo> users
    		)
    {
        this.users = users;
    }
	

	/**
	 * @return the users
	 */
	public List<UserInfo> getUsers() {
		return users;
	}


	/**
	 * @param users the users to set
	 */
	public void setUsers(List<UserInfo> users) {
		this.users = users;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Users [" + (users != null ? "users=" + users.toString() : "") + "]";
	}


	@Override
	public Iterator<UserInfo> iterator() {
		// TODO Auto-generated method stub
		return users.iterator();
	}
	
}
