package com.istack.cloud.identity.common;

import com.fasterxml.jackson.annotation.JsonCreator;
//import com.fasterxml.jackson.annotation.JsonRootName;
import com.fasterxml.jackson.annotation.JsonProperty;

//@JsonRootName("token")
public class Token {
	
	private String issuedTime;
	private String expireTime;
	private String id;
	private Tenant tenant;
	private String[] auditIds;
	
	@JsonCreator
	public Token(	@JsonProperty("issued_at") String issuedTime, 
					@JsonProperty("expires") String expireTime, 
					@JsonProperty("id") String id, 
					@JsonProperty("tenant") Tenant tenant, 
					@JsonProperty("audit_ids") String[] auditIds) {
		this.issuedTime = issuedTime;
		this.expireTime = expireTime;
		this.id = id;
		this.tenant = tenant;
		this.auditIds = auditIds;
	}
	
	public String getIssuedTime()	{ return issuedTime; }
	public String getExpireTime()	{ return expireTime; }
	public String getId()			{ return id; }
	public Tenant getTenant()		{ return tenant; }
	public String[] getAuditIds()	{ return auditIds; }
	
	

}
