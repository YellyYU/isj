package com.istack.cloud.identity.common;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Role {
	private String description;
	private String id;
	private String name;
	
	@JsonCreator
	public Role(@JsonProperty("id") String id, @JsonProperty("name") String name, @JsonProperty("description") String description) {
		this.id = id;
		this.name = name;
		this.description = description;
	}
	
	public String getId()	{ return id; }
	public String getName()	{ return name; }
	public String getDescription()	{ return description; }
}
