package com.istack.cloud.identity.common;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class User{
	@JsonProperty("user")
	private UserInfo userInfo;

	@JsonCreator
	public User(
			@JsonProperty("user") UserInfo userInfo
			)
	{
		this.userInfo = userInfo;
	}

	/**
	 * @return the userinfo
	 */
	public UserInfo getUserInfo() {
		return userInfo;
	}

	/**
	 * @param userinfo the userinfo to set
	 */
	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "User [" + (userInfo != null ? userInfo.toString() : "") + "]";
	}

	
}

