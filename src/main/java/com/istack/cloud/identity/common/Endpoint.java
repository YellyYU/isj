package com.istack.cloud.identity.common;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;

public class Endpoint {
	private String name;
	
	private String adminURL;
	
	private String region;
	
	private String internalURL;
	
	private String id;
	
	private String publicURL;
	
	@JsonCreator
	public Endpoint(@JsonProperty("name") String name,
					@JsonProperty("adminURL") String adminURL, 
					@JsonProperty("region") String region, 
					@JsonProperty("internalURL") String internalURL, 
					@JsonProperty("id") String id, 
					@JsonProperty("publicURL") String publicURL) {
		this.name = name;
		this.adminURL = adminURL;
		this.region = region;
		this.internalURL = internalURL;
		this.id = id;
		this.publicURL = publicURL;
	}
	
	public String getName()		{ return name; }
	public String getAdminURL()		{ return adminURL; }
	public String getInternalURL()	{ return internalURL; }
	public String getPublicURL()	{ return publicURL; }
	public String getId()			{ return id; }
	public String getRegion()		{ return region; }

	@Override
	public String toString() {
		return "Endpoint [name=" + name + ", adminURL=" + adminURL + ", region=" + region + ", internalURL="
				+ internalURL + ", id=" + id + ", publicURL=" + publicURL + "]";
	}
	

}
