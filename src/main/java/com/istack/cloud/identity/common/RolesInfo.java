package com.istack.cloud.identity.common;

import java.util.Arrays;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class RolesInfo {
	@JsonProperty("roles")
	private Role[] roles;
	
	@JsonProperty("roles_links")
	private Object[] rolesLinks;
	
	@JsonCreator
	public RolesInfo( 
			@JsonProperty("roles") Role[] roles,
			@JsonProperty("roles_links") Object[] rolesLinks
			)
	{
		this.roles = roles;
		this.rolesLinks = rolesLinks;
	}

	/**
	 * @return the roles
	 */
	public Role[] getRoles() {
		return roles;
	}

	/**
	 * @param roles the roles to set
	 */
	public void setRoles(Role[] roles) {
		this.roles = roles;
	}

	/**
	 * @return the rolesLinks
	 */
	public Object[] getRolesLinks() {
		return rolesLinks;
	}

	/**
	 * @param rolesLinks the rolesLinks to set
	 */
	public void setRolesLinks(Object[] rolesLinks) {
		this.rolesLinks = rolesLinks;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "RolesInfo [" + (roles != null ? "roles=" + Arrays.toString(roles) + ", " : "")
				+ (rolesLinks != null ? "rolesLinks=" + Arrays.toString(rolesLinks) : "") + "]";
	}
}



