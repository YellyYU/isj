package com.istack.cloud.identity.common;

import com.fasterxml.jackson.annotation.JsonCreator;
//import com.fasterxml.jackson.annotation.JsonRootName;
import com.fasterxml.jackson.annotation.JsonProperty;

//@JsonRootName("tenant")
public class Tenant {
	private String description;
	private Boolean enabled;
	private String id;
	private String name;
	
	@JsonCreator
	public Tenant(	@JsonProperty("description") String description, 
					@JsonProperty("enabled") Boolean enabled, 
					@JsonProperty("id") String id, 
					@JsonProperty("name") String name) {
		this.description = description;
		this.enabled = enabled;
		this.id = id;
		this.name = name;
	}
	
	public String getDescription() { return description; }
	public Boolean getEnabled() { return enabled; }
	public String getId() { return id; }
	public String getName() {return name; }

	@Override
	public String toString() {
		return "Tenant [description=" + description + ", enabled=" + enabled + ", id=" + id + ", name=" + name + "]";
	}
	
}
