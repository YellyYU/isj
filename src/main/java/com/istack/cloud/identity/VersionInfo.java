package com.istack.cloud.identity;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class VersionInfo {
	private VersionValue versionValue;
	
	@JsonCreator
	public VersionInfo(@JsonProperty("version") VersionValue versionValue) {
		this.versionValue = versionValue;
	}
	
	public VersionValue getVersion() {
		return versionValue;
	}

}
