package com.istack.cloud.identity;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

class MediaType {
	
	private String base;
	private String type;
	
	@JsonCreator
	public MediaType(@JsonProperty("base") String base, @JsonProperty("type") String type) {
		this.base = base;
		this.type = type;
	}
	
	public String getBase() {
		return base;
	}
	
	public String getType() {
		return type;
	}

}
