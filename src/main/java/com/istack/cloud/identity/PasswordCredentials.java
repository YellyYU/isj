package com.istack.cloud.identity;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

class PasswordCredentials {

	@JsonProperty("username")
	private String username;
	@JsonProperty("password")
	private String password; 
	
	//@JsonCreator
	PasswordCredentials(String userName, String password) {
		
		this.username = userName;
		this.password = password;
	}

	String getUsername() {
		return username;
	}

	String getPassword() {
		return password;
	}

}
