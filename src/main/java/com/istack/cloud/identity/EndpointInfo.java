package com.istack.cloud.identity;

import java.util.Arrays;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.istack.cloud.identity.common.Endpoint;
import com.istack.cloud.identity.common.Role;

public class EndpointInfo {
	private String endpoints_links[];
	private Endpoint endpoints[];
	
	@JsonCreator
	public EndpointInfo(@JsonProperty("endpoints_links") String[] endpoints_links, @JsonProperty("endpoints") Endpoint[] endpoints) {
		this.endpoints_links = endpoints_links;
		this.endpoints = endpoints;
	}
		
	public Endpoint[] getEndpoints() {
		return endpoints;
	}
		
	public String[] getEndpointsLinks() {
		return endpoints_links;
	}

	@Override
	public String toString() {
		return "EndpointInfo [endpoints_links=" + Arrays.toString(endpoints_links) + ", endpoints="
				+ Arrays.toString(endpoints) + "]";
	}
	
}
