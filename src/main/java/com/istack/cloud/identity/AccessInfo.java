package com.istack.cloud.identity;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.istack.cloud.identity.common.Token;
import com.istack.cloud.identity.common.User;

public class AccessInfo {
	
	private Token token;
	private ServiceCatalog[] serviceCatalog;
	private User user;
	private Map<String, Object> metadata;
	
	@JsonCreator
	AccessInfo( @JsonProperty("token") Token token, 
					@JsonProperty("serviceCatalog") ServiceCatalog[] serviceCatalog,
					@JsonProperty("user") User user,
					@JsonProperty("metadata") Map<String, Object> metadata) {
		this.token = token;
		this.serviceCatalog = serviceCatalog;
		this.user = user;
		this.metadata = metadata;
	}
	
	Token getToken() { return token; }
	ServiceCatalog[] getServiceCatalog() { return serviceCatalog; }
	User getUser() { return user; }
	public Map<String, Object> getMetadata() { return metadata; }
	
	
}
