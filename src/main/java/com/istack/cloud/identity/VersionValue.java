package com.istack.cloud.identity;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

class VersionValue {
	
	private String id;
	private Link[] links;
	private MediaType[] mediaTypes;
	private String status;
	private String updated;
	
	@JsonCreator
	public VersionValue(@JsonProperty("status") String status,
						@JsonProperty("updated") String updated,
						@JsonProperty("id") String id,
						@JsonProperty("media-types") MediaType[] mediaTypes,
						@JsonProperty("links") Link[] links)
	{
		this.status = status;
		this.updated = updated;
		this.id = id;
		this.mediaTypes = mediaTypes;
		this.links = links;
		
	}
	
	public String getId() {
		return id;
	}
	
	public String getStatue() {
		return status;
	}
	
	public String getUpdated() {
		return updated;
	}
	
	public Link[] getLinks() {
		return links;
	}
	
	public MediaType[] getMediaTypes() {
		return mediaTypes;
	}
	
	

}
