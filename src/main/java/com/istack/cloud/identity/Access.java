package com.istack.cloud.identity;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.istack.cloud.identity.common.Endpoint;
import com.istack.cloud.identity.common.Token;
import com.istack.cloud.identity.common.User;


public class Access {
		
	private AccessInfo access;
	
	@JsonCreator
	public Access(@JsonProperty("access") AccessInfo access) {
		this.access = access;
	}
	
	public AccessInfo getAccess()	{ return access; }	
	
	public Token getToken() {
		return access.getToken();
	}

}
