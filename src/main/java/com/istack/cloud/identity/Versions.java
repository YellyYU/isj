package com.istack.cloud.identity;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Versions {
	 
	private VersionValues versions;
	
	@JsonCreator
	public Versions(@JsonProperty("versions") VersionValues versions) {
		this.versions = versions;
	}
	
	public VersionValues getVersion() {
		return versions;
	}

}
