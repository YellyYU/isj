package com.istack.cloud.identity;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.istack.cloud.identity.common.Endpoint;

class ServiceCatalog {
	private Endpoint[] endpoints;
	private String[] endpointsLinks;
	private String type;
	private String name;
	
	@JsonCreator
	ServiceCatalog(	@JsonProperty("endpoints") Endpoint[] endpoints, 
							@JsonProperty("endpoints_links") String[] endpointsLinks, 
							@JsonProperty("type") String type, 
							@JsonProperty("name") String name) {
		this.endpoints = endpoints;
		this.endpointsLinks = endpointsLinks;
		this.type = type;
		this.name = name;
	}
	
	Endpoint[] getEndpoints()		{ return endpoints; }
	String[] getEndpointsLinks()	{ return endpointsLinks; }
	String getType()				{ return type; }
	String getName()				{ return name; }

}
