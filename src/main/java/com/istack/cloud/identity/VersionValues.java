package com.istack.cloud.identity;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

class VersionValues {
	private VersionValue[] values;
	
	@JsonCreator
	VersionValues(@JsonProperty("values") VersionValue[] values) {
		this.values = values;
	}
	
	public VersionValue[] getValues() {
		return values;
	}


}
