package com.istack.cloud.identity;

import java.util.Arrays;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.istack.cloud.identity.common.Tenant;

public class TenantListInfo {
	private Tenant tenants[];
	private String tenants_links[];
	
	@JsonCreator
	public TenantListInfo(@JsonProperty("tenants") Tenant[] tenants, @JsonProperty("tenants_links") String[] tenants_links) {
		this.tenants = tenants;
		this.tenants_links = tenants_links;
	}
	
	public Tenant[] getTenants() {
		return tenants;
	}
	
	public String[] getTenantsLinks() {
		return tenants_links;
	}

	@Override
	public String toString() {
		return "TenantListInfo [tenants=" + Arrays.toString(tenants) + ", tenants_links="
				+ Arrays.toString(tenants_links) + "]";
	}
}
