package com.istack.cloud.identity;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.istack.cloud.identity.common.Role;

public class RoleListInfo {
	private Role roles[];
	private String roles_links[];
		
	@JsonCreator
	public RoleListInfo(@JsonProperty("roles") Role[] roles, @JsonProperty("roles_links") String[] roles_links) {
		this.roles = roles;
		this.roles_links = roles_links;
	}
		
	public Role[] getRoles() {
		return roles;
	}
		
	public String[] getRolesLinks() {
		return roles_links;
	}
}
