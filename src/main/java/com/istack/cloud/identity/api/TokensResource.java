package com.istack.cloud.identity.api;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.web.client.RestTemplate;

import com.istack.cloud.base.*;
import com.istack.cloud.identity.Access;
import com.istack.cloud.identity.Login;

public class TokensResource {
	
	private String apiURL;
	
	public TokensResource(String apiURL) {
		this.apiURL = apiURL;
	}
	
	public Auth auth(Login login) {
		return new Auth(login);
	}
	public class Auth extends OpenStackRequest<Login, Access> {
		public Auth(Login login) {
			super(apiURL, HttpMethod.POST, "", login, Access.class);
		}
	}
}
