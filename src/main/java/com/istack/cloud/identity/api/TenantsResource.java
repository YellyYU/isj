package com.istack.cloud.identity.api;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;

import com.istack.cloud.base.OpenStackRequest;
import com.istack.cloud.base.TokenProvider;
import com.istack.cloud.identity.TenantInfo;
import com.istack.cloud.identity.TenantListInfo;
import com.istack.cloud.identity.common.Tenant;

public class TenantsResource {
	private String apiURL;
	
	public TenantsResource(String apiURL) {
		this.apiURL = apiURL;
	}
	
	public List list(){
		return new List();
	}
	
	public class List extends OpenStackRequest<Object, TenantListInfo> {
		public List() {
			super(apiURL, HttpMethod.GET, "", null, TenantListInfo.class);
		}
	}
}
