package com.istack.cloud.blockstorage;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.istack.cloud.blockstorage.common.Link;
import com.istack.cloud.blockstorage.common.QoSSpec;

public class QoSRes {
	private QoSSpec qos_specs;
	private Link links[];
	
	@JsonCreator
	public QoSRes(@JsonProperty("qos_specs") QoSSpec qos_specs,
			@JsonProperty("links") Link[] links) {
		this.qos_specs = qos_specs;
		this.links = links;
	}
	
	public QoSSpec getQoSSpec()	{ return qos_specs; }	
	public Link[] getLink()	{ return links; }	

}
