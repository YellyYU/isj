package com.istack.cloud.blockstorage;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class QoSSpecsComplete {
	private QoSSpecs qosspecs;
	private Link[] links;
	
	@JsonCreator
	public QoSSpecsComplete(@JsonProperty("qosspecs") QoSSpecs qosspecs, @JsonProperty("links") Link[] links)
	{
		this.qosspecs = qosspecs;
		this.links = links;
	}
	
	public QoSSpecs getQosSpecs() {
		return qosspecs;
	}
	
	public Link[] getLinks() {
		return this.links;
	}
}