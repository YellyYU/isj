package com.istack.cloud.blockstorage;

import com.fasterxml.jackson.annotation.JsonProperty;

public class OSResetStatus {
	private class OSResetStatusInfo{
		
		@JsonProperty("status")
		private String status;
		
		@JsonProperty("attach_status")
		private String attach_status;
		
		@JsonProperty("migration_statu")
		private String migration_status;
		
		//@JsonCreator
		OSResetStatusInfo(String status, String attach_status, String migration_status){
			this.status = status;
			this.attach_status = attach_status;
			this.migration_status = migration_status;
		}
		
		String getStatus() {
			return status;
		}
		
		String getAttachStatus() {
			return attach_status;
		}
		
		String getMigrationStatus() {
			return migration_status;
		}
	}
	
	@JsonProperty("os-reset_status")
	private OSResetStatusInfo reset_status;
	
	public OSResetStatus(String status, String attach_status, String migration_status) {
		reset_status = new OSResetStatusInfo(status, attach_status, migration_status);
	}
	
	public OSResetStatusInfo getOSResetStatusInfo() {
		return reset_status;
	}
}
