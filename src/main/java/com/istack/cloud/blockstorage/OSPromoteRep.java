package com.istack.cloud.blockstorage;

import com.fasterxml.jackson.annotation.JsonProperty;

public class OSPromoteRep {
	private class OSPromoteRepInfo{
		//@JsonCreator
		OSPromoteRepInfo(){}
	}
	
	@JsonProperty("os-promote-replica")
	private OSPromoteRepInfo replica;
	
	public OSPromoteRep() {
		replica = new OSPromoteRepInfo();
	}
	
	public OSPromoteRepInfo getOSPromoteRepInfo() {
		return replica;
	}
}
