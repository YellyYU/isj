package com.istack.cloud.blockstorage;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.istack.cloud.blockstorage.common.Volume;

public class BackupList {
	private Volume backups[];
	
	@JsonCreator
	public BackupList(@JsonProperty("backups") Volume[] backups)
	{
		this.backups = backups;
	}
	
	Volume[] getBackups(){return backups;}

}
