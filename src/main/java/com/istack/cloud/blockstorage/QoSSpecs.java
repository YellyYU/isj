package com.istack.cloud.blockstorage;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class QoSSpecs {
	private Specs specs;
	private String consumer;
	private String name;
	private String id;
	
	@JsonCreator
	public QoSSpecs(@JsonProperty("specs") Specs specs, @JsonProperty("consumer") String consumer, @JsonProperty("name") String name, @JsonProperty("id") String id)
	{
		this.specs = specs;
		this.consumer = consumer;
		this.name = name;
		this.id = id;
	}
	
	public Specs getSpecs() {
		return specs;
	}
	
	public String getConsumer() {
		return consumer;
	}
	
	public String getName() {
		return name;
	}
	
	public String getId() {
		return id;
	}
}