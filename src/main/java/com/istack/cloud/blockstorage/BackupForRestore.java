package com.istack.cloud.blockstorage;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class BackupForRestore {
	private class RestoreInfo {
		@JsonProperty("name")
		private String name;
		@JsonProperty("volume_id")
		private String volume_id;
		
		@JsonCreator
		RestoreInfo( @JsonProperty("name") String name, 
						@JsonProperty("volume_id") String volume_id) {
			this.name = name;
			this.volume_id = volume_id;
		}
		
		String getName(){ return name;}
		String getVolume_id(){ return volume_id;}
	}
	private RestoreInfo restore;
	
	@JsonCreator
	public BackupForRestore(@JsonProperty("restore") RestoreInfo restore) {
		this.restore = restore;
	}
	
	public RestoreInfo getRestore()	{ return restore; }	

}
