package com.istack.cloud.blockstorage;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class QoSSpecsCreator {
	private QoSSpecsInput specsinput;
	
	@JsonCreator
	public QoSSpecsCreator(@JsonProperty("specsinput") QoSSpecsInput specsinput)
	{
		this.specsinput = specsinput;
	}
	
	public QoSSpecsInput getSpecsinput() {
		return specsinput;
	}
}