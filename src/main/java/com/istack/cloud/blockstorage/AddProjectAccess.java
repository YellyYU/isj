package com.istack.cloud.blockstorage;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.istack.cloud.blockstorage.common.ProjectAccess;

public class AddProjectAccess {
	@JsonProperty("addProjectAccess")
	private ProjectAccess projectAccess;
	
	public AddProjectAccess(ProjectAccess projectAccess) {
		this.projectAccess = projectAccess;
	}
	
	public ProjectAccess getProjectAccess()	{ return projectAccess; }	
}
