package com.istack.cloud.blockstorage;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.istack.cloud.blockstorage.common.BackupDetail;

public class BackupsByListDetail {
	private BackupDetail backups[];
	
	@JsonCreator
	public BackupsByListDetail(@JsonProperty("backups") BackupDetail[] backups)
	{
		this.backups = backups;
	}
	
	BackupDetail[] getBackupsDetail(){return backups;}

}
