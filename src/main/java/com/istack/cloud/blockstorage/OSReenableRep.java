package com.istack.cloud.blockstorage;

import com.fasterxml.jackson.annotation.JsonProperty;

public class OSReenableRep {
	private class OSReenableRepInfo{
		//@JsonCreator
		OSReenableRepInfo(){}
	}
	
	@JsonProperty("os-reenable-replica")
	private OSReenableRepInfo replica;
	
	public OSReenableRep() {
		replica = new OSReenableRepInfo();
	}
	
	public OSReenableRepInfo getOSReenableRepInfo() {
		return replica;
	}
}
