package com.istack.cloud.blockstorage;

import com.fasterxml.jackson.annotation.JsonProperty;

public class VolumeForUpdate {
	private class SimpleVolume{
		
		@JsonProperty("name")
		private String name;
		
		@JsonProperty("description")
		private String description;
		
		//@JsonCreator
		SimpleVolume(String name, String description){
			this.name = name;
			this.description = description;
		}
		
		String getName() {return name;}
		String getDescription() {return description;}
	}
	
	@JsonProperty("volume")
	private SimpleVolume volume;
	
	public VolumeForUpdate(SimpleVolume volume) {
		this.volume = volume;
	}
	
	public SimpleVolume getVolume() {
		return volume;
	}
}
