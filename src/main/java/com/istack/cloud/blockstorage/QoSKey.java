package com.istack.cloud.blockstorage;

import com.fasterxml.jackson.annotation.JsonProperty;

public class QoSKey {
	private class QoSKeyInfo{
		@JsonProperty("delay")
		private String delay;
		
		//@JsonCreator
		QoSKeyInfo(String delay){
			this.delay = delay;
		}
		
		String getdelay() {return delay;}
	}
	
	@JsonProperty("qos_specs")
	private QoSKeyInfo qos_specs;
	
	public QoSKey(QoSKeyInfo qos_specs) {
		this.qos_specs = qos_specs;
	}
	
	public QoSKeyInfo getQoSKey() {
		return qos_specs;
	}

}
