package com.istack.cloud.blockstorage;

import com.fasterxml.jackson.annotation.JsonProperty;

public class OSUnmanage {
	private class OSUnmanageInfo{
		//@JsonCreator
		OSUnmanageInfo(){}
	}
	
	@JsonProperty("os-unmanage")
	private OSUnmanageInfo unmanage;
	
	public OSUnmanage() {
		unmanage = new OSUnmanageInfo();
	}
	
	public OSUnmanageInfo getOSUnmanageInfo() {
		return unmanage;
	}
}
