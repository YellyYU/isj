package com.istack.cloud.blockstorage;

import com.fasterxml.jackson.annotation.JsonProperty;

public class OSRemoveImageMetadata {

	private class OSRemoveImageMetadataInfo{
	
		@JsonProperty("key")
		private String key;
		
		//@JsonCreator
		OSRemoveImageMetadataInfo(String key){
			this.key = key;
		}
		
		String getKey() {
			return key;
		}
	}
	
	@JsonProperty("os-unset_image_metadata")
	private OSRemoveImageMetadataInfo imageKey;
	
	public OSRemoveImageMetadata(String key) {
		imageKey = new OSRemoveImageMetadataInfo(key);
	}
	
	public OSRemoveImageMetadataInfo getOSRemoveImageMetadataInfo() {
		return imageKey;
	}
}
