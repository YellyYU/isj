package com.istack.cloud.blockstorage;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.istack.cloud.blockstorage.common.QoSSpec;

public class QoSByList {
	private QoSSpec qos_specs[];
	
	@JsonCreator
	public QoSByList(@JsonProperty("qos_specs") QoSSpec[] qos_specs) {
		this.qos_specs = qos_specs;
	}
	
	public QoSSpec[] getQoSSpec()	{ return qos_specs; }	
}
