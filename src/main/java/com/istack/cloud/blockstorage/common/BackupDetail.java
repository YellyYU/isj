package com.istack.cloud.blockstorage.common;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class BackupDetail {
	private String availability_zone;
	private String container;
	private String created_at;
	private String description;
	private String fail_reason;
	private String id;
	private Link links[];
	private String name;
	private Integer object_count;
	private Integer size;
	private String status;
	private String volume_id;
	private boolean is_incremental;
	private boolean has_dependent_backups;
	
	@JsonCreator
	BackupDetail( @JsonProperty("status") String status, 
					@JsonProperty("availability_zone") String availability_zone,
					@JsonProperty("container") String container,
					@JsonProperty("created_at") String created_at,
					@JsonProperty("description") String description,
					@JsonProperty("links") Link[] links,
					@JsonProperty("fail_reason") String fail_reason,
					@JsonProperty("id") String id,
					@JsonProperty("name") String name,
					@JsonProperty("object_count") Integer object_count,
					@JsonProperty("size") Integer size,
					@JsonProperty("volume_id") String volume_id,
					@JsonProperty("is_incremental") boolean is_incremental,
					@JsonProperty("has_dependent_backups") boolean has_dependent_backups) {
		this.status = status;
		this.container=container;
		this.links=links;
		this.availability_zone= availability_zone;
		this.created_at= created_at;
		this.description= description;
		this.fail_reason= fail_reason;
		this.volume_id= volume_id;
		this.name= name;
		this.object_count= object_count;
		this.id= id;
		this.size= size;
		this.is_incremental= is_incremental;
		this.has_dependent_backups= has_dependent_backups;
	}
	
	String getStatus(){ return status;}
	String getContainer(){ return container;}
	String getCreated_at(){ return created_at;}
	String getDescription(){ return description;}
	Link[] getLinks(){ return links;}
	String getAvailability_zone(){ return availability_zone;}
	String getFail_reason(){ return fail_reason;}
	String getVolume_id(){ return volume_id;}
	String getName(){ return name;}
	Integer getObject_count(){ return object_count;}
	Integer getSize(){ return size;}
	String getId(){ return id;}
	boolean getIs_incremental(){ return is_incremental;}
	boolean getHas_dependent_backups(){ return has_dependent_backups;}

}
