package com.istack.cloud.blockstorage.common;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Metadata1 {
	private MetadataInfo metadata;
	
	@JsonCreator
	public Metadata1(@JsonProperty("metadata") MetadataInfo metadata) {
		this.metadata = metadata;
	}
	
	public MetadataInfo getMetadata()	{ return metadata; }	
}
