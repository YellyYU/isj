package com.istack.cloud.blockstorage.common;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class QoSSpec {
	private class QoSSpecInfo{
		private String numberOfFailures;
		private String availability;
		
		@JsonCreator
		QoSSpecInfo( @JsonProperty("numberOfFailures") String numberOfFailures, 
						@JsonProperty("availability") String availability) {
			this.numberOfFailures = numberOfFailures;
			this.availability = availability;
		}
		
		String getnumberOfFailures(){ return numberOfFailures;}
		String getavailability(){ return availability;}
	}
	
	private QoSSpecInfo specs;
	private String consumer;
	private String name;
	private String id;
	
	@JsonCreator
	public QoSSpec(@JsonProperty("specs") QoSSpecInfo specs,
					@JsonProperty("consumer") String consumer,
					@JsonProperty("name") String name,
					@JsonProperty("id") String id) {
		this.specs = specs;
		this.consumer = consumer;
		this.name = name;
		this.id = id;
	}
	
	public QoSSpecInfo getQoSSpecInfo()	{ return specs; }	
	public String getConsumer()	{ return consumer; }	
	public String getName()	{ return name; }	
	public String getId()	{ return id; }	

}
