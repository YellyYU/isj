package com.istack.cloud.blockstorage.common;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class MetadataInfo {

	private String name;
	
	@JsonCreator
	public MetadataInfo(){}
	
	@JsonCreator
	public MetadataInfo(@JsonProperty("name") String name)
	{
		this.name = name;
	}
	
	String getName(){ return name;}
}
