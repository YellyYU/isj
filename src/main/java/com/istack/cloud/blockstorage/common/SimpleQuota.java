package com.istack.cloud.blockstorage.common;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SimpleQuota {
	private class SimpleQuotaInfo{
		@JsonProperty("snapshots")
		private Integer snapshots;
		
		//@JsonCreator
		SimpleQuotaInfo(Integer snapshots){
			this.snapshots = snapshots;
		}
		
		Integer getSnapshots() {return snapshots;}
	}
	
	@JsonProperty("quota_set")
	private SimpleQuotaInfo quota_set;
	
	public SimpleQuota(SimpleQuotaInfo quota_set) {
		this.quota_set = quota_set;
	}
	
	public SimpleQuotaInfo getQuota() {
		return quota_set;
	}

}
