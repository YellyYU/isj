package com.istack.cloud.blockstorage.common;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ProjectAccess {
	@JsonProperty("project")
	private String project;
	
	public ProjectAccess(String project)
	{
		this.project = project;
	}
	
	String getProject(){ return project;}
}
