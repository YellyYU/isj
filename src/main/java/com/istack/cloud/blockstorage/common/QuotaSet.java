package com.istack.cloud.blockstorage.common;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class QuotaSet {
	private class QuotaSetInfo{
		private Integer gigabytes;
		private Integer snapshots;
		private Integer volumes;
		
		@JsonCreator
		QuotaSetInfo( @JsonProperty("gigabytes") Integer gigabytes, 
						@JsonProperty("snapshots") Integer snapshots, 
						@JsonProperty("volume_id") Integer volume_id) {
			this.gigabytes = gigabytes;
			this.snapshots = snapshots;
			this.volumes = volumes;
		}
		
		Integer getGigabytes(){ return gigabytes;}
		Integer getSnapshots(){ return snapshots;}
		Integer getVolumes(){ return volumes;}
	}
	
	private QuotaSetInfo quota_set;
	
	@JsonCreator
	public QuotaSet(@JsonProperty("quota_set") QuotaSetInfo quota_set) {
		this.quota_set = quota_set;
	}
	
	public QuotaSetInfo getQuota_set()	{ return quota_set; }	
	
}
