package com.istack.cloud.blockstorage.common;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Volume {
	private String id;
	private Link links[];
	private String name;
	
	@JsonCreator
	public Volume(@JsonProperty("id") String id, 
				@JsonProperty("links") Link[] links,
				@JsonProperty("name") String name)
	{
		this.id = id;
		this.links = links;
		this.name = name;
	}
	
	String getId(){return id;}
	Link[] getLinks(){return links;}
	String getName(){return name;}
}
