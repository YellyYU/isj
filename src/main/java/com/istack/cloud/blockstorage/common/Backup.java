package com.istack.cloud.blockstorage.common;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Backup {
	private Volume backup;
	
	@JsonCreator
	public Backup(@JsonProperty("backup") Volume backup)
	{
		this.backup = backup;
	}
	
	Volume getBackup(){return backup;}
}
