package com.istack.cloud.blockstorage.common;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Metadata {
	private String readonly;
	private String attached_mode;
	
	@JsonCreator
	public Metadata(){}
	
	@JsonCreator
	public Metadata(@JsonProperty("readonly") String readonly, 
					@JsonProperty("attached_mode") String attached_mode)
	{
		this.readonly = readonly;
		this.attached_mode = attached_mode;
	}
	
	String getReadonly(){ return readonly;}
	String getAttached_mode(){ return attached_mode;}
	
}
