package com.istack.cloud.blockstorage.common;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class VolumeDetail {
	private String status;
	private String migration_status;
	private String user_id;
	private Attachment attachments[];
	private Link links[];
	private String availability_zone;
	private String os_vol_host_attr_host;
	private String bootable;
	private boolean encrypted;
	private String os_volume_replication_extended_status;
	private String os_vol_tenant_attr_tenant_id;
	private String os_vol_mig_status_attr_migstat;
	private String created_at;
	private String description;
	private String os_volume_replication_driver_data;
	private String volume_type;
	private String name;
	private String replication_status;
	private String consistencygroup_id;
	private String os_vol_mig_status_attr_name_id;
	private String source_volid;
	private String snapshot_id;
	private boolean multiattach;
	private Metadata metadata;
	private String id;
	private Integer size;
	
	@JsonCreator
	VolumeDetail( @JsonProperty("status") String status, 
					@JsonProperty("migration_status") String migration_status,
					@JsonProperty("user_id") String user_id,
					@JsonProperty("os-vol-host-attr:host") String os_vol_host_attr_host,
					@JsonProperty("attachments") Attachment[] attachments,
					@JsonProperty("links") Link[] links,
					@JsonProperty("availability_zone") String availability_zone,
					@JsonProperty("bootable") String bootable,
					@JsonProperty("encrypted") boolean encrypted,
					@JsonProperty("os-volume-replication:extended_status") String os_volume_replication_extended_status,
					@JsonProperty("os-vol-tenant-attr:tenant_id") String os_vol_tenant_attr_tenant_id,
					@JsonProperty("os-vol-mig-status-attr:migstat") String os_vol_mig_status_attr_migstat,
					@JsonProperty("created_at") String created_at,
					@JsonProperty("description") String description,
					@JsonProperty("os-volume-replication:driver_data") String os_volume_replication_driver_data,
					@JsonProperty("volume_type") String volume_type,
					@JsonProperty("name") String name,
					@JsonProperty("replication_status") String replication_status,
					@JsonProperty("consistencygroup_id") String consistencygroup_id,
					@JsonProperty("os-vol-mig-status-attr:name_id") String os_vol_mig_status_attr_name_id,
					@JsonProperty("source_volid") String source_volid,
					@JsonProperty("snapshot_id") String snapshot_id,
					@JsonProperty("multiattach") boolean multiattach,
					@JsonProperty("metadata") Metadata metadata,
					@JsonProperty("id") String id,
					@JsonProperty("size") Integer size) {
		this.status = status;
		this.migration_status = migration_status;
		this.user_id= user_id;
		this.os_vol_host_attr_host= os_vol_host_attr_host;
		this.attachments= attachments;
		this.links=links;
		this.availability_zone= availability_zone;
		this.bootable= bootable;
		this.encrypted= encrypted;
		this.os_volume_replication_extended_status= os_volume_replication_extended_status;
		this.os_vol_tenant_attr_tenant_id= os_vol_tenant_attr_tenant_id;
		this.os_vol_mig_status_attr_migstat= os_vol_mig_status_attr_migstat;
		this.created_at= created_at;
		this.description= description;
		this.os_volume_replication_driver_data= os_volume_replication_driver_data;
		this.volume_type= volume_type;
		this.name= name;
		this.replication_status= replication_status;
		this.consistencygroup_id= consistencygroup_id;
		this.os_vol_mig_status_attr_name_id= os_vol_mig_status_attr_name_id;
		this.source_volid= source_volid;
		this.snapshot_id= snapshot_id;
		this.multiattach= multiattach;
		this.metadata= metadata;
		this.id= id;
		this.size= size;
	}
	
	String getStatus(){ return status;}
	String getMigration_status(){ return migration_status;}
	String getUser_id(){ return user_id;}
	String getOs_vol_host_attr_host(){ return os_vol_host_attr_host;}
	Attachment[] getAttachments(){ return attachments;}
	Link[] getLinks(){ return links;}
	String getAvailability_zone(){ return availability_zone;}
	String getBootable(){ return bootable;}
	boolean getEncrypted(){ return encrypted;}
	String getOs_volume_replication_extended_status(){ return os_volume_replication_extended_status;}
	String getOs_vol_tenant_attr_tenant_id(){ return os_vol_tenant_attr_tenant_id;}
	String getOs_vol_mig_status_attr_migstat(){ return os_vol_mig_status_attr_migstat;}
	String getOs_vol_mig_status_attr_name_id(){ return os_vol_mig_status_attr_name_id;}
	String getCreated_at(){ return created_at;}
	String getDescription(){ return description;}
	String getVolume_type(){ return volume_type;}
	String getName(){ return name;}
	String getReplication_status(){ return replication_status;}
	String getConsistencygroup_id(){ return consistencygroup_id;}
	String getSource_volid(){ return source_volid;}
	String getSnapshot_id(){ return snapshot_id;}
	boolean getMultiattach(){ return multiattach;}
	Metadata getMetadata(){ return metadata;}
	String getId(){ return id;}
	Integer getSize(){ return size;}
}
