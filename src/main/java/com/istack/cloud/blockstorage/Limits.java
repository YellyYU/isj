package com.istack.cloud.blockstorage;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Limits {
	private class LimitsInfo {
		private class Absolute {
			private Integer totalSnapshotsUsed;
			private Integer maxTotalBackups;
			private Integer maxTotalVolumeGigabytes;
			private Integer maxTotalSnapshots;
			private Integer maxTotalBackupGigabytes;
			private Integer totalBackupGigabytesUsed;
			private Integer maxTotalVolumes;
			private Integer totalVolumesUsed;
			private Integer totalBackupsUsed;
			private Integer totalGigabytesUsed;
			
			@JsonCreator
			Absolute( @JsonProperty("totalSnapshotsUsed") Integer totalSnapshotsUsed, 
							@JsonProperty("maxTotalBackups") Integer maxTotalBackups,
							@JsonProperty("maxTotalVolumeGigabytes") Integer maxTotalVolumeGigabytes,
							@JsonProperty("maxTotalSnapshots") Integer maxTotalSnapshots,
							@JsonProperty("maxTotalBackupGigabytes") Integer maxTotalBackupGigabytes,
							@JsonProperty("totalBackupGigabytesUsed") Integer totalBackupGigabytesUsed,
							@JsonProperty("totalVolumesUsed") Integer totalVolumesUsed,
							@JsonProperty("totalBackupsUsed") Integer totalBackupsUsed,
							@JsonProperty("maxTotalVolumes") Integer maxTotalVolumes,
							@JsonProperty("totalGigabytesUsed") Integer totalGigabytesUsed) {
				this.totalSnapshotsUsed = totalSnapshotsUsed;
				this.maxTotalBackups = maxTotalBackups;
				this.maxTotalVolumeGigabytes = maxTotalVolumeGigabytes;
				this.maxTotalSnapshots = maxTotalSnapshots;
				this.maxTotalBackupGigabytes = maxTotalBackupGigabytes;
				this.totalBackupGigabytesUsed = totalBackupGigabytesUsed;
				this.totalVolumesUsed = totalVolumesUsed;
				this.totalBackupsUsed = totalBackupsUsed;
				this.maxTotalVolumes = maxTotalVolumes;
				this.totalGigabytesUsed = totalGigabytesUsed;
			}
			
			Integer getTotalSnapshotsUsed(){ return totalSnapshotsUsed;}
			Integer getMaxTotalBackups(){ return maxTotalBackups;}
			Integer getMaxTotalVolumeGigabytes(){ return maxTotalVolumeGigabytes;}
			Integer getMaxTotalSnapshots(){ return maxTotalSnapshots;}
			Integer getMaxTotalBackupGigabytes(){ return maxTotalBackupGigabytes;}
			Integer getTotalBackupGigabytesUsed(){ return totalBackupGigabytesUsed;}
			Integer getTotalVolumesUsed(){ return totalVolumesUsed;}
			Integer getTotalBackupsUsed(){ return totalBackupsUsed;}
			Integer getMaxTotalVolumes(){ return maxTotalVolumes;}
			Integer getTotalGigabytesUsed(){ return totalGigabytesUsed;}
		}
		
		private String rate[];
		private Absolute absolute;
		
		@JsonCreator
		LimitsInfo( @JsonProperty("rate") String[] rate, 
						@JsonProperty("absolute") Absolute absolute) {
			this.rate = rate;
			this.absolute = absolute;
		}
		
		String[] getRate(){ return rate;}
		
		Absolute getAbsolute(){ return absolute;	}
	}
	private LimitsInfo limits;
	
	@JsonCreator
	public Limits(@JsonProperty("limits") LimitsInfo limits) {
		this.limits = limits;
	}
	
	public LimitsInfo getLimits()	{ return limits; }	
}
