package com.istack.cloud.blockstorage;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ProjectAccessDetail {
	private String typeId;
	private String projectId;
	
	@JsonCreator
	public ProjectAccessDetail(@JsonProperty("volume_type_id") String typeId, 
					@JsonProperty("project_id") String projectId)
	{
		this.typeId = typeId;
		this.projectId = projectId;
	}
	
	String getTypeId(){ return typeId;}
	String getProjectId(){ return projectId;}
}
