package com.istack.cloud.blockstorage;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Specs {
	private int availability;
	private int numberOfFailures;
	
	@JsonCreator
	public Specs(@JsonProperty("availability") int availability, @JsonProperty("numberOfFailures") int numberOfFailures)
	{
		this.availability = availability;
		this.numberOfFailures = numberOfFailures;
	}
	
	public int getAvailability() {
		return availability;
	}
	
	public int getNumberOfFailures() {
		return numberOfFailures;
	}
}