package com.istack.cloud.blockstorage;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.istack.cloud.blockstorage.common.Attachment;
import com.istack.cloud.blockstorage.common.Link;
import com.istack.cloud.blockstorage.common.Metadata;

public class GeneralVolumeInfo {
	private String status;
	private String migration_status;
	private String user_id;
	private Attachment attachments[];
	private Link links[];
	private String availability_zone;
	private String bootable;
	private boolean encrypted;
	private String created_at;
	private String description;
	private String updated_at;
	private String volume_type;
	private String name;
	private String replication_status;
	private String consistencygroup_id;
	private String source_volid;
	private String snapshot_id;
	private boolean multiattach;
	private Metadata metadata;
	private String id;
	private Integer size;
	
	@JsonCreator
	GeneralVolumeInfo( @JsonProperty("status") String status, 
					@JsonProperty("migration_status") String migration_status,
					@JsonProperty("user_id") String user_id,
					@JsonProperty("attachments") Attachment[] attachments,
					@JsonProperty("links") Link[] links,
					@JsonProperty("availability_zone") String availability_zone,
					@JsonProperty("bootable") String bootable,
					@JsonProperty("encrypted") boolean encrypted,
					@JsonProperty("created_at") String created_at,
					@JsonProperty("description") String description,
					@JsonProperty("updated_at") String updated_at,
					@JsonProperty("volume_type") String volume_type,
					@JsonProperty("name") String name,
					@JsonProperty("replication_status") String replication_status,
					@JsonProperty("consistencygroup_id") String consistencygroup_id,
					@JsonProperty("source_volid") String source_volid,
					@JsonProperty("snapshot_id") String snapshot_id,
					@JsonProperty("multiattach") boolean multiattach,
					@JsonProperty("metadata") Metadata metadata,
					@JsonProperty("id") String id,
					@JsonProperty("size") Integer size) {
		this.status = status;
		this.migration_status = migration_status;
		this.user_id= user_id;
		this.attachments= attachments;
		this.links=links;
		this.availability_zone= availability_zone;
		this.bootable= bootable;
		this.encrypted= encrypted;
		this.created_at= created_at;
		this.description= description;
		this.updated_at= updated_at;
		this.volume_type= volume_type;
		this.name= name;
		this.replication_status= replication_status;
		this.consistencygroup_id= consistencygroup_id;
		this.source_volid= source_volid;
		this.snapshot_id= snapshot_id;
		this.multiattach= multiattach;
		this.metadata= metadata;
		this.id= id;
		this.size= size;
	}
	
	String getStatus(){ return status;}
	String getMigration_status(){ return migration_status;}
	String getUser_id(){ return user_id;}
	Attachment[] getAttachments(){ return attachments;}
	Link[] getLinks(){ return links;}
	String getAvailability_zone(){ return availability_zone;}
	String getBootable(){ return bootable;}
	boolean getEncrypted(){ return encrypted;}
	String getCreated_at(){ return created_at;}
	String getUpdated_at(){ return updated_at;}
	String getDescription(){ return description;}
	String getVolume_type(){ return volume_type;}
	String getName(){ return name;}
	String getReplication_status(){ return replication_status;}
	String getConsistencygroup_id(){ return consistencygroup_id;}
	String getSource_volid(){ return source_volid;}
	String getSnapshot_id(){ return snapshot_id;}
	boolean getMultiattach(){ return multiattach;}
	Metadata getMetadata(){ return metadata;}
	String getId(){ return id;}
	Integer getSize(){ return size;}
}
