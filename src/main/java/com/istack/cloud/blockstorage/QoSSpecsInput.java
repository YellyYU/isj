package com.istack.cloud.blockstorage;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;


public class QoSSpecsInput {
	private int availability;
	private String name;
	private int numberOfFailures;
	
	@JsonCreator
	public QoSSpecsInput(@JsonProperty("availability") int availability, @JsonProperty("name") String name, @JsonProperty("numberOfFailures") int numberOfFailures)
	{
		this.availability = availability;
		this.name = name;
		this.numberOfFailures = numberOfFailures;
	}
	
	public int getAvailability() {
		return availability;
	}
	
	public String getName() {
		return name;
	}
	
	public int getNumberOfFailures() {
		return numberOfFailures;
	}
}