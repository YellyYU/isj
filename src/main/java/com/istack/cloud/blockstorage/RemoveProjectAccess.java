package com.istack.cloud.blockstorage;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.istack.cloud.blockstorage.common.ProjectAccess;

public class RemoveProjectAccess {
	@JsonProperty("removeProjectAccess")
	private ProjectAccess projectAccess;
	
	public RemoveProjectAccess(ProjectAccess projectAccess) {
		this.projectAccess = projectAccess;
	}
	
	public ProjectAccess getProjectAccess()	{ return projectAccess; }
}
