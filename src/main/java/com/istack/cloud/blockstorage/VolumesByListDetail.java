package com.istack.cloud.blockstorage;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.istack.cloud.blockstorage.common.VolumeDetail;

public class VolumesByListDetail {
	private VolumeDetail volumes[];
	
	@JsonCreator
	public VolumesByListDetail(@JsonProperty("volumes") VolumeDetail[] volumes) {
		this.volumes = volumes;
	}
	
	public VolumeDetail[] getVolumes()	{ return volumes; }	
}
