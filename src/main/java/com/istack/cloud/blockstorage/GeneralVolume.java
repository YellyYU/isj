package com.istack.cloud.blockstorage;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class GeneralVolume {
	private GeneralVolumeInfo volume;
	
	@JsonCreator
	public GeneralVolume(@JsonProperty("volume") GeneralVolumeInfo volume) {
		this.volume = volume;
	}
	
	public GeneralVolumeInfo getVolume()	{ return volume; }	
}
