package com.istack.cloud.blockstorage;

import com.fasterxml.jackson.annotation.JsonProperty;

public class OSExtend {
	private class OSExtendInfo{
	
		@JsonProperty("new_size")
		private Integer newSize;
		
		//@JsonCreator
		OSExtendInfo(Integer newSize){
			this.newSize = newSize;
		}
		
		Integer getNewSize() {
			return newSize;
		}
	}
	
	@JsonProperty("os-extend")
	private OSExtendInfo os_extend;
	
	public OSExtend(Integer size) {
		os_extend = new OSExtendInfo(size);
	}
	
	public OSExtendInfo getOSExtendInfo() {
		return os_extend;
	}
}
