package com.istack.cloud.blockstorage;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class BackupForCreate {
	private class BackupForCreateInfo {
		@JsonProperty("container")
		private String container;
		@JsonProperty("description")
		private String description;
		@JsonProperty("name")
		private String name;
		@JsonProperty("volume_id")
		private String volume_id;
		@JsonProperty("incremental")
		private boolean incremental;
		
		//@JsonCreator
		public BackupForCreateInfo(String container,
				String description,
				String name,
				String volume_id,
				boolean incremental) {
			this.container = container;
			this.description = description;
			this.name = name;
			this.volume_id = volume_id;
			this.incremental = incremental;
		}
		
		String getContainer(){return container;}
		String getDescription(){return description;}
		String getName(){return name;}
		String getVolume_id(){return volume_id;}
		boolean getIncremental(){return incremental;}		
	}

	private BackupForCreateInfo backup;
	
	@JsonCreator
	public BackupForCreate(@JsonProperty("backup") BackupForCreateInfo backup) {
		this.backup = backup;
	}
	
	public BackupForCreateInfo getBackup()	{ return backup; }
}
