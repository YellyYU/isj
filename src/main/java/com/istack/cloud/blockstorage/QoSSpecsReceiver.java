package com.istack.cloud.blockstorage;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class QoSSpecsReceiver {
	private QoSSpecs[] qosspecs;
	
	@JsonCreator
	public QoSSpecsReceiver(@JsonProperty("qosspecs") QoSSpecs[] qosspecs)
	{
		this.qosspecs = qosspecs;
	}
	
	public QoSSpecs[] getAllQoSSpecs() {
		return this.qosspecs;
	}
}