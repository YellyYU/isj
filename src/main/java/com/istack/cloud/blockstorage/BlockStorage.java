package com.istack.cloud.blockstorage;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import com.fasterxml.jackson.databind.SerializationFeature;
import com.istack.cloud.blockstorage.api.LimitsResource;
import com.istack.cloud.blockstorage.api.VolumesResource;

public class BlockStorage {
	private String apiURL;
	private HttpHeaders header;				
	private HttpEntity<?> request;			
	private RestTemplate restTemplate;
	
	private final LimitsResource LIMITS;
	private final VolumesResource VOLUMES;
	
	public BlockStorage(String url, String token) {
		apiURL = url;
		header = new HttpHeaders();				// generate a default header containing only the X-Auth-Token
		header.set("X-Auth-Token", token);
		request = new HttpEntity(header);		// generate a default request
		restTemplate = new RestTemplate();
		
		LIMITS = new LimitsResource(apiURL);
		VOLUMES = new VolumesResource(apiURL);
	}
	
	public LimitsResource limits(){ return LIMITS;	}
	public VolumesResource volumes(){ return VOLUMES;	}

	/**
     * Quality of Service: 
     * POST		/v2/{tenant_id}/qos-specs		Create QoS
     * @param tenant_id
     * @param availability
     * @param name
     * @param numberOfFailures
     * @return QoSSpecsComplete
     */
	public QoSSpecsComplete createQoS(String tenant_id, int availability, String name, int numberOfFailures)
	{
		QoSSpecsInput specsinput = new QoSSpecsInput(availability, name, numberOfFailures);
		QoSSpecsCreator qoscreator = new QoSSpecsCreator(specsinput);
		request = new HttpEntity<Object>(qoscreator, header);
		return restTemplate.exchange(apiURL + "v2/" + tenant_id + "/qos-specs", HttpMethod.GET, request, QoSSpecsComplete.class).getBody();
	}
	
	/**
     * Quality of Service: 
     * GET		/v2/{tenant_id}/qos-specs		List QoS
     * @param tenant_id
     * @return QoSSpecs[]
     */
	public QoSSpecs[] getAllQoSSpecs(String tenant_id)
	{
		QoSSpecsReceiver received = restTemplate.exchange(apiURL + "v2/" + tenant_id + "/qos-specs", HttpMethod.GET, request, QoSSpecsReceiver.class).getBody();
		return received.getAllQoSSpecs();
	}
	
	/**
     * Quality of Service: 
     * GET		/v2/{tenant_id}/qos-specs/{qos_id}		Show QoS details
     * @param tenant_id
     * @param qos_id
     * @return QoSSpecsComplete
     */
	public QoSSpecsComplete getQoSSpecs(String tenant_id, String qos_id)
	{
		QoSSpecsComplete qosspecs = restTemplate.exchange(apiURL + "v2/" + tenant_id + "/qos-specs/" + qos_id, HttpMethod.GET, request, QoSSpecsComplete.class).getBody();
		return qosspecs;
	}
	
	/**
     * Quality of Service: 
     * DELETE		/v2/{tenant_id}/qos-specs/{qos_id}		Delete QoS
     * @param tenant_id
     * @param qos_id
     */
	public void deleteQoSSpecs(String tenant_id, String qos_id)
	{
		restTemplate.delete(apiURL + "v2/" + tenant_id + "/qos-specs/" + qos_id);
	}

}