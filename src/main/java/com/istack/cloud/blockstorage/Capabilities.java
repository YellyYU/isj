package com.istack.cloud.blockstorage;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Capabilities {
	private class Properties{
		private class Property{
			private String title;
			private String description;
			private String type;
		
			@JsonCreator
			Property( @JsonProperty("title") String title, 
					@JsonProperty("description") String description, 
					@JsonProperty("type") String type) {
				this.title = title;
				this.description = description;
				this.type = type;
			}
		
			String getTitle(){ return title;}
			String getDescription(){ return description;}
			String getType(){ return type;}
		}
		
		private Property compression;
		private Property qos;
		private Property replication;
		private Property thin_provisioning;
		
		@JsonCreator
		public Properties(@JsonProperty("compression") Property compression,
				@JsonProperty("qos") Property qos,
				@JsonProperty("replication") Property replication,
				@JsonProperty("thin_provisioning") Property thin_provisioning) {
			this.compression = compression;
			this.qos = qos;
			this.replication = replication;
			this.thin_provisioning = thin_provisioning;
		}
		
		public Property getCompression() { return compression; }
		public Property getQos() { return qos; }	
		public Property getReplication() { return replication; }	
		public Property getThin_provisioning() { return thin_provisioning; }	
	}
	
	private String namespace;
	private String vendor_name;
	private String volume_backend_name;
	private String pool_name;
	private String driver_version;
	private String storage_protocol;
	private String display_name;
	private String description;
	private String visibility;
	private Properties properties;
	
	@JsonCreator
	public Capabilities(@JsonProperty("namespace") String namespace,
						@JsonProperty("vendor_name") String vendor_name,
						@JsonProperty("volume_backend_name") String volume_backend_name,
						@JsonProperty("pool_name") String pool_name,
						@JsonProperty("driver_version") String driver_version,
						@JsonProperty("storage_protocol") String storage_protocol,
						@JsonProperty("display_name") String display_name,
						@JsonProperty("description") String description,
						@JsonProperty("visibility") String visibility,
						@JsonProperty("properties") Properties properties) {
		this.namespace = namespace;
		this.vendor_name = vendor_name;
		this.volume_backend_name = volume_backend_name;
		this.pool_name = pool_name;
		this.driver_version = driver_version;
		this.storage_protocol = storage_protocol;
		this.display_name = display_name;
		this.description = description;
		this.visibility = visibility;
		this.properties = properties;
	}
	
	public String getNamespace()	{ return namespace; }	
	public String getVendor_name()	{ return vendor_name; }	
	public String getVolume_backend_name()	{ return volume_backend_name; }	
	public String getPool_name()	{ return pool_name; }	
	public String getDriver_version()	{ return driver_version; }	
	public String getStorage_protocol()	{ return storage_protocol; }	
	public String getDisplay_name()	{ return display_name; }	
	public String getDescription()	{ return description; }	
	public Properties getProperties()	{ return properties; }	

}
