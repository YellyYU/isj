package com.istack.cloud.blockstorage;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ListProjectAccess {
	private ProjectAccessDetail access;
	
	@JsonCreator
	public ListProjectAccess(@JsonProperty("volume_type_access") ProjectAccessDetail access) {
		this.access = access;
	}
	
	public ProjectAccessDetail getAccess()	{ return access; }	
}
