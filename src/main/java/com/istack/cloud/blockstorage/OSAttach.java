package com.istack.cloud.blockstorage;

import com.fasterxml.jackson.annotation.JsonProperty;

public class OSAttach {

	private class OSAttachInfo{
	
		@JsonProperty("instance_uuid")
		private String instance_uuid;

		@JsonProperty("mountpoint")
		private String mountpoint;
		
		//@JsonCreator
		OSAttachInfo(String instance_uuid, String mountpoint){
			this.instance_uuid = instance_uuid;
			this.mountpoint = mountpoint;
		}
		
		String getInstanceUuid() {
			return instance_uuid;
		}
		
		String getMountpoint() {
			return mountpoint;
		}
	}
	
	@JsonProperty("os-attach")
	private OSAttachInfo volume;
	
	public OSAttach(String instance_uuid, String mountpoint) {
		volume = new OSAttachInfo(instance_uuid, mountpoint);
	}
	
	public OSAttachInfo getOSAttachInfo() {
		return volume;
	}
}
