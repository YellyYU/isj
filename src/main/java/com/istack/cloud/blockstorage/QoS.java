package com.istack.cloud.blockstorage;

import com.fasterxml.jackson.annotation.JsonProperty;

public class QoS {
	private class QoSInfo{

		@JsonProperty("availability")
		private String availability;
		
		@JsonProperty("name")
		private String name;
		
		@JsonProperty("numberOfFailures")
		private String numberOfFailures;
		
		//@JsonCreator
		QoSInfo(String name, String availability, String numberOfFailures){
			this.name = name;
			this.availability = availability;
			this.numberOfFailures = numberOfFailures;
		}
		
		String getName() {return name;}
		String getAvailability() {return availability;}
		String getNumberOfFailures() {return numberOfFailures;}
	}
	
	@JsonProperty("qos_specs")
	private QoSInfo qos_specs;
	
	public QoS(QoSInfo qos_specs) {
		this.qos_specs = qos_specs;
	}
	
	public QoSInfo getQoSInfo() {
		return qos_specs;
	}
}
