package com.istack.cloud.blockstorage;

import com.fasterxml.jackson.annotation.JsonProperty;

public class OSSetImageMetadata {
	private class OSSetImageMetadataInfo{
		private class ImageMetadata{
			
			@JsonProperty("image_id")
			private String image_id;

			@JsonProperty("image_name")
			private String image_name;

			@JsonProperty("kernel_id")
			private String kernel_id;

			@JsonProperty("ramdisk_id")
			private String ramdisk_id;
			
			//@JsonCreator
			ImageMetadata(String image_id, String image_name, String kernel_id, String ramdisk_id){
				this.image_id = image_id;
				this.image_name = image_name;
				this.kernel_id = kernel_id;
				this.ramdisk_id = ramdisk_id;
			}
			
			String getImageId() {
				return image_id;
			}
			
			String getImageName() {
				return image_name;
			}

			String getKernelId() {
				return kernel_id;
			}

			String getRamdiskId() {
				return ramdisk_id;
			}
		}
		
		@JsonProperty("metadata")
		private ImageMetadata metadata;
		
		public OSSetImageMetadataInfo(String image_id, String image_name, String kernel_id, String ramdisk_id) {
			metadata = new ImageMetadata(image_id, image_name, kernel_id, ramdisk_id);
		}
		
		public ImageMetadata getImageMetadata() {
			return metadata;
		}
	}
	
	@JsonProperty("os-set_image_metadata")
	private OSSetImageMetadataInfo imageMetadata;
	
	public OSSetImageMetadata(String image_id, String image_name, String kernel_id, String ramdisk_id) {
		imageMetadata = new OSSetImageMetadataInfo(image_id, image_name, kernel_id, ramdisk_id);
	}
	
	public OSSetImageMetadataInfo getOSSetImageMetadataInfo() {
		return imageMetadata;
	}
}
