package com.istack.cloud.blockstorage;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class BackupByRestore {
	private class RestoreInfo{
		private String backup_id;
		private String volume_id;
		
		@JsonCreator
		RestoreInfo( @JsonProperty("backup_id") String backup_id, 
						@JsonProperty("volume_id") String volume_id) {
			this.backup_id = backup_id;
			this.volume_id = volume_id;
		}
		
		String getBackup(){ return backup_id;}
		String getVolume(){ return volume_id;}
	}
	private RestoreInfo restore;
	
	@JsonCreator
	public BackupByRestore(@JsonProperty("restore") RestoreInfo restore) {
		this.restore = restore;
	}
	
	public RestoreInfo getRestore()	{ return restore; }	
}
