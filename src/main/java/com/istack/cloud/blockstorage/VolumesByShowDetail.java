package com.istack.cloud.blockstorage;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.istack.cloud.blockstorage.common.VolumeDetail;

public class VolumesByShowDetail {
	private VolumeDetail volume;
	
	@JsonCreator
	public VolumesByShowDetail(@JsonProperty("volume") VolumeDetail volume) {
		this.volume = volume;
	}
	
	public VolumeDetail getVolume()	{ return volume; }	
}
