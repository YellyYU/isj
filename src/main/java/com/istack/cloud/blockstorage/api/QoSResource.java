package com.istack.cloud.blockstorage.api;

import org.springframework.http.HttpMethod;

import com.istack.cloud.base.OpenStackRequest;
import com.istack.cloud.blockstorage.GeneralVolume;
import com.istack.cloud.blockstorage.QoS;
import com.istack.cloud.blockstorage.QoSByList;
import com.istack.cloud.blockstorage.QoSKey;
import com.istack.cloud.blockstorage.QoSRes;
import com.istack.cloud.blockstorage.VolumeForCreate;
import com.istack.cloud.blockstorage.VolumeForUpdate;
import com.istack.cloud.blockstorage.VolumesByList;
import com.istack.cloud.blockstorage.VolumesByListDetail;
import com.istack.cloud.blockstorage.VolumesByShowDetail;
import com.istack.cloud.blockstorage.api.VolumesResource.Create;
import com.istack.cloud.blockstorage.api.VolumesResource.Delete;
import com.istack.cloud.blockstorage.api.VolumesResource.List;
import com.istack.cloud.blockstorage.api.VolumesResource.ListDetail;
import com.istack.cloud.blockstorage.api.VolumesResource.ShowDetail;
import com.istack.cloud.blockstorage.api.VolumesResource.Update;
import com.istack.cloud.blockstorage.common.QoSSpec;

public class QoSResource {
	private String apiURL;
	//String tenantId;
	
	public QoSResource(String apiURL){
		this.apiURL = apiURL;
		//this.tenantId = tenantId;
	}
	
	public Create create(String tenantId, QoS qos){
		return new Create(tenantId, qos);
	}
	
	public List list(String tenantId){
		return new List(tenantId);
	}
	
	public ShowDetail showDetail(String tenantId, String qosId){
		return new ShowDetail(tenantId, qosId);
	}
	
	public SetKey setKey(String tenantId, String qosId, QoSKey key){
		return new SetKey(tenantId, qosId, key);
	}

	public class Create extends OpenStackRequest<QoS, QoSRes> {
		public Create(String tenantId, QoS qos) {
			super(apiURL, HttpMethod.POST, "/v2/"+tenantId+"/qos-specs", qos, QoSRes.class);
		}
	}
	
	public class List extends OpenStackRequest<Object, QoSByList> {
		public List(String tenantId) {
			super(apiURL, HttpMethod.GET, "/v2/"+tenantId+"/qos-specs", null, QoSByList.class);
		}
	}
	
	public class ShowDetail extends OpenStackRequest<Object, QoSRes> {
		public ShowDetail(String tenantId, String qosId) {
			super(apiURL, HttpMethod.GET, "/v2/"+tenantId+"/qos-specs"+qosId, null, QoSRes.class);
		}
	}
	
	public class SetKey extends OpenStackRequest<QoSKey, QoSKey> {
		public SetKey(String tenantId, String qosId, QoSKey key) {
			super(apiURL, HttpMethod.PUT, "/v2/"+tenantId+"/qos-specs"+qosId, key, QoSKey.class);
		}
	}
}
