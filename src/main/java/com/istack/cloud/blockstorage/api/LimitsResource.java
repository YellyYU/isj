package com.istack.cloud.blockstorage.api;


import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import com.istack.cloud.base.OpenStackRequest;
import com.istack.cloud.base.TokenProvider;
import com.istack.cloud.blockstorage.Limits;
import com.istack.cloud.blockstorage.LimitsInfo;

public class LimitsResource {
	private String apiURL;
	//String tenantId;
	
	public LimitsResource(String apiURL){
		this.apiURL = apiURL;
		//this.tenantId = tenantId;
	}
	
	public Show show(String tenantId){
		return new Show(tenantId);
	}
	
	public class Show extends OpenStackRequest<Object, Limits> {
		public Show(String tenantId) {
			super(apiURL, HttpMethod.GET, "/v2/"+tenantId+"/limits", null, Limits.class);
		}
	}
}