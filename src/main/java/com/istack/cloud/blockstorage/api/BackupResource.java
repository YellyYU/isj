package com.istack.cloud.blockstorage.api;

import org.springframework.http.HttpMethod;

import com.istack.cloud.base.OpenStackRequest;
import com.istack.cloud.blockstorage.BackupByRestore;
import com.istack.cloud.blockstorage.BackupByShowDetail;
import com.istack.cloud.blockstorage.BackupForCreate;
import com.istack.cloud.blockstorage.BackupForRestore;
import com.istack.cloud.blockstorage.BackupList;
import com.istack.cloud.blockstorage.BackupsByListDetail;
import com.istack.cloud.blockstorage.OSForceDelete;
import com.istack.cloud.blockstorage.common.Backup;
import com.istack.cloud.blockstorage.common.Volume;

public class BackupResource {
	private String apiURL;
	//String tenantId;
	
	public BackupResource(String apiURL){
		this.apiURL = apiURL;
		//this.tenantId = tenantId;
	}
	
	public Create create(String tenantId, BackupForCreate backup){
		return new Create(tenantId, backup);
	}
	
	public List list(String tenantId){
		return new List(tenantId);
	}
	
	public ListDetail listDetail(String tenantId){
		return new ListDetail(tenantId);
	}
	
	public ShowDetail showDetail(String tenantId, String backupId){
		return new ShowDetail(tenantId, backupId);
	}
	
	public Restore restore(String tenantId, String volumeId, BackupForRestore restore){
		return new Restore(tenantId, volumeId, restore);
	}
	
	public Delete delete(String tenantId, String backupId){
		return new Delete(tenantId, backupId);
	}
	
	public ForceDelete forceDelete(String tenantId, String backupId, OSForceDelete force_delete){
		return new ForceDelete(tenantId, backupId, force_delete);
	}
	
	public class Create extends OpenStackRequest<BackupForCreate, Backup> {
		public Create(String tenantId, BackupForCreate backup) {
			super(apiURL, HttpMethod.POST, "/v2/"+tenantId+"/backups", backup, Backup.class);
		}
	}
	
	public class List extends OpenStackRequest<Object, BackupList> {
		public List(String tenantId) {
			super(apiURL, HttpMethod.GET, "/v2/"+tenantId+"/backups", null, BackupList.class);
		}
	}
	
	public class ListDetail extends OpenStackRequest<Object, BackupsByListDetail> {
		public ListDetail(String tenantId) {
			super(apiURL, HttpMethod.GET, "/v2/"+tenantId+"/backups/detail", null, BackupsByListDetail.class);
		}
	}
	
	public class ShowDetail extends OpenStackRequest<Object, BackupByShowDetail> {
		public ShowDetail(String tenantId, String backupId) {
			super(apiURL, HttpMethod.GET, "/v2/"+tenantId+"/backups/"+backupId, null, BackupByShowDetail.class);
		}
	}
	
	public class Restore extends OpenStackRequest<BackupForRestore, BackupByRestore> {
		public Restore(String tenantId, String backupId, BackupForRestore restore) {
			super(apiURL, HttpMethod.POST, "/v2/"+tenantId+"/backups/"+backupId+"/restore", restore, BackupByRestore.class);
		}
	}
	
	public class Delete extends OpenStackRequest<Object, Object> {
		public Delete(String tenantId, String backupId) {
			super(apiURL, HttpMethod.DELETE, "/v2/"+tenantId+"/backups/"+backupId, null, null);
		}
	}
	
	public class ForceDelete extends OpenStackRequest<OSForceDelete, Object> {
		public ForceDelete(String tenantId, String backupId, OSForceDelete force_delete) {
			super(apiURL, HttpMethod.POST, "/v2/"+tenantId+"/backups/"+backupId+"/action", force_delete, null);
		}
	}
}
