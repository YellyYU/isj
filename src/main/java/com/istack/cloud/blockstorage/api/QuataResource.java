package com.istack.cloud.blockstorage.api;

import org.springframework.http.HttpMethod;

import com.istack.cloud.base.OpenStackRequest;
import com.istack.cloud.blockstorage.common.QuotaSet;
import com.istack.cloud.blockstorage.common.SimpleQuota;
import com.istack.cloud.blockstorage.common.SimpleQuotaRes;

public class QuataResource {
	private String apiURL;
	//String tenantId;
	
	public QuataResource(String apiURL){
		this.apiURL = apiURL;
		//this.tenantId = tenantId;
	}
	
	public Show show(String tenantId_admin, String tenantId){
		return new Show(tenantId_admin, tenantId);
	}
	
	public Update update(String tenantId_admin, String tenantId, SimpleQuota quota){
		return new Update(tenantId_admin, tenantId, quota);
	}
	
	public Delete delete(String tenantId_admin, String tenantId){
		return new Delete(tenantId_admin, tenantId);
	}
	
	public Get get(String tenantId){
		return new Get(tenantId);
	}
	
	public ShowForUser showForUser(String tenantId_admin, String tenantId, String userId){
		return new ShowForUser(tenantId_admin, tenantId, userId);
	}
	
	public UpdateForUser updateForUser(String tenantId_admin, String tenantId, String userId, SimpleQuota quota){
		return new UpdateForUser(tenantId_admin, tenantId, userId, quota);
	}
	
	public DeleteForUser deleteForUser(String tenantId_admin, String tenantId, String userId){
		return new DeleteForUser(tenantId_admin, tenantId, userId);
	}
	
	public ShowDetailForUser showDetailForUser(String tenantId_admin, String tenantId, String userId){
		return new ShowDetailForUser(tenantId_admin, tenantId, userId);
	}
	
	public class Show extends OpenStackRequest<Object, QuotaSet> {
		public Show(String tenantId_admin, String tenantId) {
			super(apiURL, HttpMethod.GET, "/v2/"+tenantId_admin+"/os-quota-sets/"+tenantId, null, QuotaSet.class);
		}
	}
	
	public class Update extends OpenStackRequest<SimpleQuota, SimpleQuota> {
		public Update(String tenantId_admin, String tenantId, SimpleQuota quota) {
			super(apiURL, HttpMethod.PUT, "/v2/"+tenantId_admin+"/os-quota-sets/"+tenantId, quota, SimpleQuota.class);
		}
	}
	
	public class Delete extends OpenStackRequest<Object, Object> {
		public Delete(String tenantId_admin, String tenantId) {
			super(apiURL, HttpMethod.DELETE, "/v2/"+tenantId_admin+"/os-quota-sets/"+tenantId, null, null);
		}
	}
	
	public class Get extends OpenStackRequest<Object, QuotaSet> {
		public Get(String tenantId) {
			super(apiURL, HttpMethod.GET, "/v2/"+tenantId+"/os-quota-sets/defaults", null, QuotaSet.class);
		}
	}
	
	public class ShowForUser extends OpenStackRequest<Object, SimpleQuota> {
		public ShowForUser(String tenantId_admin, String tenantId, String userId) {
			super(apiURL, HttpMethod.GET, "/v2/"+tenantId_admin+"/os-quota-sets/"+tenantId+"/"+userId, null, SimpleQuota.class);
		}
	}
	
	public class UpdateForUser extends OpenStackRequest<SimpleQuota, SimpleQuota> {
		public UpdateForUser(String tenantId_admin, String tenantId, String userId, SimpleQuota quota) {
			super(apiURL, HttpMethod.PUT, "/v2/"+tenantId_admin+"/os-quota-sets/"+tenantId+"/"+userId, quota, SimpleQuota.class);
		}
	}
	
	public class DeleteForUser extends OpenStackRequest<Object, Object> {
		public DeleteForUser(String tenantId_admin, String tenantId, String userId) {
			super(apiURL, HttpMethod.DELETE, "/v2/"+tenantId_admin+"/os-quota-sets/"+tenantId+"/"+userId, null, null);
		}
	}
	
	public class ShowDetailForUser extends OpenStackRequest<Object, SimpleQuota> {
		public ShowDetailForUser(String tenantId_admin, String tenantId, String userId) {
			super(apiURL, HttpMethod.GET, "/v2/"+tenantId_admin+"/os-quota-sets/"+tenantId+"/detail/"+userId, null, SimpleQuota.class);
		}
	}
}
