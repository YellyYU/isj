package com.istack.cloud.blockstorage.api;

import org.springframework.http.HttpMethod;

import com.istack.cloud.base.OpenStackRequest;
import com.istack.cloud.blockstorage.AddProjectAccess;
import com.istack.cloud.blockstorage.GeneralVolume;
import com.istack.cloud.blockstorage.ListProjectAccess;
import com.istack.cloud.blockstorage.OSAttach;
import com.istack.cloud.blockstorage.OSDetach;
import com.istack.cloud.blockstorage.OSExtend;
import com.istack.cloud.blockstorage.OSPromoteRep;
import com.istack.cloud.blockstorage.OSReenableRep;
import com.istack.cloud.blockstorage.OSRemoveImageMetadata;
import com.istack.cloud.blockstorage.OSResetStatus;
import com.istack.cloud.blockstorage.OSSetImageMetadata;
import com.istack.cloud.blockstorage.OSUnmanage;
import com.istack.cloud.blockstorage.RemoveProjectAccess;
import com.istack.cloud.blockstorage.VolumeForCreate;
import com.istack.cloud.blockstorage.VolumeForUpdate;
import com.istack.cloud.blockstorage.VolumesByList;
import com.istack.cloud.blockstorage.VolumesByListDetail;
import com.istack.cloud.blockstorage.VolumesByShowDetail;
import com.istack.cloud.blockstorage.common.Metadata1;

public class VolumesResource {
	private String apiURL;
	//String tenantId;
	
	public VolumesResource(String apiURL){
		this.apiURL = apiURL;
		//this.tenantId = tenantId;
	}
	
	public Create create(String tenantId, VolumeForCreate volume){
		return new Create(tenantId, volume);
	}
	
	public List list(String tenantId){
		return new List(tenantId);
	}
	
	public ListDetail listDetail(String tenantId){
		return new ListDetail(tenantId);
	}
	
	public ShowDetail showDetail(String tenantId, String volumeId){
		return new ShowDetail(tenantId, volumeId);
	}
	
	public Update update(String tenantId, String volumeId, VolumeForUpdate volume){
		return new Update(tenantId, volumeId, volume);
	}
	
	public Delete delete(String tenantId, String volumeId){
		return new Delete(tenantId, volumeId);
	}
	
	public CreateMetadata createMetadata(String tenantId, String volumeId, Metadata1 metadata){
		return new CreateMetadata(tenantId, volumeId, metadata);
	}
	
	public ShowMetadata showMetadata(String tenantId, String volumeId){
		return new ShowMetadata(tenantId, volumeId);
	}
	
	public UpdateMetadata updateMetadata(String tenantId, String volumeId, Metadata1 metadata){
		return new UpdateMetadata(tenantId, volumeId, metadata);
	}
	
	public AddTypeAccess addTypeAccess(String tenantId, String volumeType, AddProjectAccess access){
		return new AddTypeAccess(tenantId, volumeType, access);
	}
	
	public RemoveTypeAccess removeTypeAccess(String tenantId, String volumeType, RemoveProjectAccess access){
		return new RemoveTypeAccess(tenantId, volumeType, access);
	}
	
	public ListTypeAccess listTypeAccess(String tenantId, String volumeType){
		return new ListTypeAccess(tenantId, volumeType);
	}
	
	public Extend extend(String tenantId, String volumeId, OSExtend os_extend){
		return new Extend(tenantId, volumeId, os_extend);
	}
	
	public ResetStatus resetStatus(String tenantId, String volumeId, OSResetStatus reset_status){
		return new ResetStatus(tenantId, volumeId, reset_status);
	}
	
	public SetImageMetadata setImageMetadata(String tenantId, String volumeId, OSSetImageMetadata image_metadata){
		return new SetImageMetadata(tenantId, volumeId, image_metadata);
	}
	
	public RemoveImageMetadata removeImageMetadata(String tenantId, String volumeId, OSRemoveImageMetadata image_metadata){
		return new RemoveImageMetadata(tenantId, volumeId, image_metadata);
	}
	
	public Attach attach(String tenantId, String volumeId, OSAttach volume){
		return new Attach(tenantId, volumeId, volume);
	}
	
	public Unmanage unmanage(String tenantId, String volumeId, OSUnmanage unmanage){
		return new Unmanage(tenantId, volumeId, unmanage);
	}
	
	public Detach detach(String tenantId, String volumeId, OSDetach detach){
		return new Detach(tenantId, volumeId, detach);
	}
	
	public PromoteRep promoteRep(String tenantId, String volumeId, OSPromoteRep replica){
		return new PromoteRep(tenantId, volumeId, replica);
	}
	
	public ReenableRep reenableeRep(String tenantId, String volumeId, OSReenableRep replica){
		return new ReenableRep(tenantId, volumeId, replica);
	}
	
	public class Create extends OpenStackRequest<VolumeForCreate, GeneralVolume> {
		public Create(String tenantId, VolumeForCreate volume) {
			super(apiURL, HttpMethod.POST, "/v2/"+tenantId+"/volumes", volume, GeneralVolume.class);
		}
	}
	
	public class List extends OpenStackRequest<Object, VolumesByList> {
		public List(String tenantId) {
			super(apiURL, HttpMethod.GET, "/v2/"+tenantId+"/volumes", null, VolumesByList.class);
		}
	}
	
	public class ListDetail extends OpenStackRequest<Object, VolumesByListDetail> {
		public ListDetail(String tenantId) {
			super(apiURL, HttpMethod.GET, "/v2/"+tenantId+"/volumes/detail", null, VolumesByListDetail.class);
		}
	}
	
	public class ShowDetail extends OpenStackRequest<Object, VolumesByShowDetail> {
		public ShowDetail(String tenantId, String volumeId) {
			super(apiURL, HttpMethod.GET, "/v2/"+tenantId+"/volumes/"+volumeId, null, VolumesByShowDetail.class);
		}
	}
	
	public class Update extends OpenStackRequest<VolumeForUpdate, GeneralVolume> {
		public Update(String tenantId, String volumeId, VolumeForUpdate volume) {
			super(apiURL, HttpMethod.PUT, "/v2/"+tenantId+"/volumes/"+volumeId, volume, GeneralVolume.class);
		}
	}
	
	public class Delete extends OpenStackRequest<Object, Object> {
		public Delete(String tenantId, String volumeId) {
			super(apiURL, HttpMethod.DELETE, "/v2/"+tenantId+"/volumes/"+volumeId, null, null);
		}
	}
	
	public class CreateMetadata extends OpenStackRequest<Metadata1, Metadata1> {
		public CreateMetadata(String tenantId, String volumeId, Metadata1 metadata) {
			super(apiURL, HttpMethod.POST, "/v2/"+tenantId+"/volumes/"+volumeId+"/metadata", metadata, Metadata1.class);
		}
	}
	
	public class ShowMetadata extends OpenStackRequest<Object, Metadata1> {
		public ShowMetadata(String tenantId, String volumeId) {
			super(apiURL, HttpMethod.GET, "/v2/"+tenantId+"/volumes/"+volumeId+"/metadata", null, Metadata1.class);
		}
	}
	
	public class UpdateMetadata extends OpenStackRequest<Metadata1, Metadata1> {
		public UpdateMetadata(String tenantId, String volumeId, Metadata1 metadata) {
			super(apiURL, HttpMethod.PUT, "/v2/"+tenantId+"/volumes/"+volumeId+"/metadata", metadata, Metadata1.class);
		}
	}
	
	public class AddTypeAccess extends OpenStackRequest<AddProjectAccess, Object> {
		public AddTypeAccess(String tenantId, String volumeType, AddProjectAccess access) {
			super(apiURL, HttpMethod.POST, "/v2/"+tenantId+"/types/"+volumeType+"/action", access, null);
		}
	}
	
	public class RemoveTypeAccess extends OpenStackRequest<RemoveProjectAccess, Object> {
		public RemoveTypeAccess(String tenantId, String volumeType, RemoveProjectAccess access) {
			super(apiURL, HttpMethod.POST, "/v2/"+tenantId+"/types/"+volumeType+"/action", access, null);
		}
	}
	
	public class ListTypeAccess extends OpenStackRequest<Object, ListProjectAccess> {
		public ListTypeAccess(String tenantId, String volumeType) {
			super(apiURL, HttpMethod.GET, "/v2/"+tenantId+"/types/"+volumeType+"/os-volume-type-access", null, ListProjectAccess.class);
		}
	}
	
	public class Extend extends OpenStackRequest<OSExtend, Object> {
		public Extend(String tenantId, String volumeId, OSExtend os_extend) {
			super(apiURL, HttpMethod.POST, "/v2/"+tenantId+"/volumes/"+volumeId+"/action", os_extend, null);
		}
	}
	
	public class ResetStatus extends OpenStackRequest<OSResetStatus, Object> {
		public ResetStatus(String tenantId, String volumeId, OSResetStatus reset_status) {
			super(apiURL, HttpMethod.POST, "/v2/"+tenantId+"/volumes/"+volumeId+"/action", reset_status, null);
		}
	}
	
	public class SetImageMetadata extends OpenStackRequest<OSSetImageMetadata, Object> {
		public SetImageMetadata(String tenantId, String volumeId, OSSetImageMetadata image_metadata) {
			super(apiURL, HttpMethod.POST, "/v2/"+tenantId+"/volumes/"+volumeId+"/action", image_metadata, null);
		}
	}
	
	public class RemoveImageMetadata extends OpenStackRequest<OSRemoveImageMetadata, Object> {
		public RemoveImageMetadata(String tenantId, String volumeId, OSRemoveImageMetadata image_metadata) {
			super(apiURL, HttpMethod.POST, "/v2/"+tenantId+"/volumes/"+volumeId+"/action", image_metadata, null);
		}
	}
	
	public class Attach extends OpenStackRequest<OSAttach, Object> {
		public Attach(String tenantId, String volumeId, OSAttach volume) {
			super(apiURL, HttpMethod.POST, "/v2/"+tenantId+"/volumes/"+volumeId+"/action", volume, null);
		}
	}
	
	public class Unmanage extends OpenStackRequest<OSUnmanage, Object> {
		public Unmanage(String tenantId, String volumeId, OSUnmanage unmagage) {
			super(apiURL, HttpMethod.POST, "/v2/"+tenantId+"/volumes/"+volumeId+"/action", unmagage, null);
		}
	}
	
	public class Detach extends OpenStackRequest<OSDetach, Object> {
		public Detach(String tenantId, String volumeId, OSDetach detach) {
			super(apiURL, HttpMethod.POST, "/v2/"+tenantId+"/volumes/"+volumeId+"/action", detach, null);
		}
	}
	
	public class PromoteRep extends OpenStackRequest<OSPromoteRep, Object> {
		public PromoteRep(String tenantId, String volumeId, OSPromoteRep replica) {
			super(apiURL, HttpMethod.POST, "/v2/"+tenantId+"/volumes/"+volumeId+"/action", replica, null);
		}
	}
	
	public class ReenableRep extends OpenStackRequest<OSReenableRep, Object> {
		public ReenableRep(String tenantId, String volumeId, OSReenableRep replica) {
			super(apiURL, HttpMethod.POST, "/v2/"+tenantId+"/volumes/"+volumeId+"/action", replica, null);
		}
	}
}
