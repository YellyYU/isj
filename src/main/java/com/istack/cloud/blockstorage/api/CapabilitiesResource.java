package com.istack.cloud.blockstorage.api;

import org.springframework.http.HttpMethod;

import com.istack.cloud.base.OpenStackRequest;
import com.istack.cloud.blockstorage.BackupByShowDetail;
import com.istack.cloud.blockstorage.Capabilities;
import com.istack.cloud.blockstorage.api.BackupResource.ShowDetail;

public class CapabilitiesResource {
	private String apiURL;
	//String tenantId;
	
	public CapabilitiesResource(String apiURL){
		this.apiURL = apiURL;
		//this.tenantId = tenantId;
	}

	public Show show(String tenantId, String hostname){
		return new Show(tenantId, hostname);
	}
	
	public class Show extends OpenStackRequest<Object, Capabilities> {
		public Show(String tenantId, String hostname) {
			super(apiURL, HttpMethod.GET, "/v2/"+tenantId+"/capabilities/"+hostname, null, Capabilities.class);
		}
	}
}
