package com.istack.cloud.blockstorage;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.istack.cloud.blockstorage.common.Metadata;

public class VolumeForCreate {
	private class VolumeForCreateInfo {
		@JsonProperty("size")
		private Integer size;
		@JsonProperty("availability_zone")
		private String availability_zone;
		@JsonProperty("source_volid")
		private String source_volid;
		@JsonProperty("description")
		private String description;
		@JsonProperty("multiattach")
		private boolean multiattach;
		@JsonProperty("snapshot_id")
		private String snapshot_id;
		@JsonProperty("name")
		private String name;
		@JsonProperty("imageRef")
		private String imageRef;
		@JsonProperty("volume_typ")
		private String volume_type;
		@JsonProperty("metadata")
		private Metadata metadata;
		@JsonProperty("source_replica")
		private String source_replica;
		@JsonProperty("consistencygroup_id")
		private String consistencygroup_id;
		
		//@JsonCreator
		public VolumeForCreateInfo(Integer size,
				String availability_zone,
				String source_volid,
				String description,
				boolean multiattach,
				String snapshot_id,
				String name,
				String imageRef,
				String volume_type,
				//Metadata metadata,
				String source_replica,
				String consistencygroup_id) {
			this.size = size;
			this.availability_zone = availability_zone;
			this.source_volid = source_volid;
			this.description = description;
			this.multiattach = multiattach;
			this.snapshot_id = snapshot_id;
			this.name = name;
			this.imageRef = imageRef;
			this.volume_type = volume_type;
			this.metadata = new Metadata();
			this.source_replica = source_replica;
			this.consistencygroup_id = consistencygroup_id;
		}
		
		Integer getSize(){return size;}
		String getAvailability_zone(){return availability_zone;}
		String getSource_volid(){return source_volid;}
		String getDescription(){return description;}
		boolean getMultiattach(){return multiattach;}
		String getSnapshot_id(){return snapshot_id;}
		String getName(){return name;}
		String getImageRef(){return imageRef;}
		String getConsistencygroup_id(){return consistencygroup_id;}
		String getVolume_type(){return volume_type;}
		Metadata getMetadata(){return metadata;}
		String getSource_replica(){return source_replica;}
		
	}
	
	private VolumeForCreateInfo volumeForCreateInfo;
	
	@JsonCreator
	public VolumeForCreate(@JsonProperty("volume") VolumeForCreateInfo volume) {
		this.volumeForCreateInfo = volume;
	}
	
	public VolumeForCreateInfo getVolume()	{ return volumeForCreateInfo; }
}
