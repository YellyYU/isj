package com.istack.cloud.blockstorage;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class OSForceDelete {
	private class OSForceDeleteInfo{}
	
	private OSForceDeleteInfo backup;
	
	@JsonCreator
	public OSForceDelete(@JsonProperty("os-force_delete") OSForceDeleteInfo backup) {
		this.backup = backup;
	}
	
	public OSForceDeleteInfo getForceDeleteInfo()	{ return backup; }
}
