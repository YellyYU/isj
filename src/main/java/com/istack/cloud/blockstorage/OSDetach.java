package com.istack.cloud.blockstorage;

import com.fasterxml.jackson.annotation.JsonProperty;

public class OSDetach {
	private class DetachInfo{
		private class Connector{
			
			@JsonProperty("initiator")
			private String initiator;
			
			//@JsonCreator
			Connector(String initiator){
				this.initiator = initiator;
			}
			
			String getInitiator() {
				return initiator;
			}
		}
		
		@JsonProperty("connector")
		private Connector connector;

		@JsonProperty("attachment_id")
		private String attachment_id;
		
		public DetachInfo(String initiator, String attachment_id) {
			this.connector = new Connector(initiator);
			this.attachment_id = attachment_id;
		}
		
		public Connector getConnector() {
			return connector;
		}
		
		String getAttachmentId() {
			return attachment_id;
		}
	}
	
	@JsonProperty("os-force_detach")
	private DetachInfo detach;
	
	public OSDetach(String initiator, String attachment_id) {
		detach = new DetachInfo(initiator, attachment_id);
	}
	
	public DetachInfo getDetachInfo() {
		return detach;
	}
}
