package com.istack.cloud.blockstorage;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.istack.cloud.blockstorage.common.BackupDetail;

public class BackupByShowDetail {
	private BackupDetail backup;
	
	@JsonCreator
	public BackupByShowDetail(@JsonProperty("backup") BackupDetail backup)
	{
		this.backup = backup;
	}
	
	BackupDetail getBackupDetail(){return backup;}

}
