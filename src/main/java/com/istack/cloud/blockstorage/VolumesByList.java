package com.istack.cloud.blockstorage;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.istack.cloud.blockstorage.common.Volume;

public class VolumesByList {
	private Volume volumes[];
	
	@JsonCreator
	public VolumesByList(@JsonProperty("volumes") Volume[] volumes) {
		this.volumes = volumes;
	}
	
	public Volume[] getVolumes()	{ return volumes; }	
}
