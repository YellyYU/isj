package com.istack.cloud.blockstorage;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Link {
	private String href;
	private String rel;
	private String type;
	
	@JsonCreator
	public Link(@JsonProperty("href") String href, @JsonProperty("rel") String rel,
				@JsonProperty("type") String type)
	{
		this.href = href;
		this.rel = rel;
		this.type = type;
	}
	
	public String getHref() {
		return href;
	}
	
	public String getRel() {
		return rel;
	}
	
	public String getType() {
		return type;
	}
}