package com.istack.cloud.base;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

public class OpenStackRequest<T, R> {
	
	private String url;
	private HttpMethod method;
	private HttpHeaders header;
	//private HttpEntity<?> entity;
	private T jsonBody;
	private Class<R> returnType;
	private ResponseEntity<R> response;
	private RestTemplate restTemplate;
	private static TokenProvider tokenProvider = new TokenProvider("");
	
	
	public OpenStackRequest() {
		restTemplate = new RestTemplate();
		
		String token = tokenProvider.getToken();
		if(!token.equals("")) {
			header.set("X-Auth-Token", token);
		}
	}
	
	public OpenStackRequest(String url, HttpMethod method, String path, T jsonBody , Class<R> returnType) {
		this.url = url+path;
		this.method = method;
		this.header = new HttpHeaders();
		//this.entity = new HttpEntity<Object>(jsonBody, header);
		this.jsonBody = jsonBody;
		this.returnType = returnType;
		this.restTemplate = new RestTemplate();
		this.response = null;
		
		String token = tokenProvider.getToken();
		if(header.isEmpty()){
			if(!token.equals("")) {
				header.set("X-Auth-Token", token);
			}
		}
		
	}
	
	public R request() {
		HttpEntity<?> entity = new HttpEntity<Object>(jsonBody, header);
		response = restTemplate.exchange(url, method, entity, returnType);
	 
		return response.getBody();		
	}
	
	public R request(HttpHeaders inputHeader){
		if(restTemplate == null)
			restTemplate = new RestTemplate();
		
		header = inputHeader;
		
		HttpEntity<?> entity = new HttpEntity<Object>(jsonBody, header);
		response = restTemplate.exchange(url, method, entity, returnType);
	 
		return response.getBody();
	}
	
	public static void setTokenProvider(String token) {
		tokenProvider.setToken(token);
	}
	
	//just for test
	public static String getToken() {
		return tokenProvider.getToken();
	}
	

}
