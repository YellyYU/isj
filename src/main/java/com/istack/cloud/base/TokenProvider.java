package com.istack.cloud.base;

public class TokenProvider {
	
	private String token;
	
	public TokenProvider(String token) {
		this.token = token;
	}
	
	public String getToken() {
		return token;
	}
	
	public void setToken(String token) {
		this.token = token;
	}

}
