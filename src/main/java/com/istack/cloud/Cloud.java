package com.istack.cloud;

import com.istack.cloud.base.OpenStackRequest;
import com.istack.cloud.blockstorage.BlockStorage;
import com.istack.cloud.identity.*;
import com.istack.cloud.identityadmin.*;
import com.istack.cloud.imageservice.*;

import org.springframework.http.HttpEntity;
import org.springframework.web.client.RestTemplate;

public class Cloud {
		
	public Identity identity;
	public IdentityAdmin identity_admin;
	public ImageService image_service;
	public BlockStorage block_storage;
	
	public Cloud(String apiURL, String username, String password, String project) {
		
		identity = new Identity(apiURL+":35000");
		identity_admin = new IdentityAdmin(apiURL + ":35357");
		//image_service = new ImageService(apiURL + ":35292", token);
		
		Login login = new Login(username, password, project);
		Access response = identity.tokens().auth(login).request();
		OpenStackRequest.setTokenProvider(response.getToken().getId());
		String token = response.getToken().getId();
		//System.out.println(response.getToken().getId());
		

		block_storage = new BlockStorage(apiURL + ":35776", token);
		
	}	
	
}
