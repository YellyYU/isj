package com.istack.cloud.imageservice;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class PropertyReceiver {
	private Properties properties;
	
	@JsonCreator
	public PropertyReceiver(@JsonProperty("properties") Properties properties) {
		this.properties = properties;
	}
	
	public Properties getProperties() {
		return properties;
	}
	

}
