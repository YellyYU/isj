package com.istack.cloud.imageservice;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class NamespaceProperty{
	private String name; 
	private String title;
	private String type;
	private String description;
	private String[] enume;
	
	@JsonCreator
	public NamespaceProperty(
			@JsonProperty("name") String name,
			@JsonProperty("title") String title,
			@JsonProperty("type") String type,
			@JsonProperty("enum") String[] enume,
			@JsonProperty("description") String description){
		
		this.name = name;
		this.title = title;
		this.type = type;
		this.enume = enume;
		this.description = description;
	}
	
	
	public String getName(){
		return name;
	}
	
	public String getTitle(){
		return title;
	}
	
	public String getType(){
		return type;
	}
	
	public String[] getEnum(){
		return enume;
	}
	public String getDescription(){
		return description;
	}
}