package com.istack.cloud.imageservice;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Properties {
	private NamespaceProperty hw_watchdog_action;
	private NamespaceProperty hypervisor_type;
	
	@JsonCreator
	public Properties(@JsonProperty("hw_watchdog_action") NamespaceProperty hw_watchdog_action,
			@JsonProperty("hypervisor_type") NamespaceProperty hypervisor_type){
		this.hw_watchdog_action = hw_watchdog_action;
		this.hypervisor_type = hypervisor_type;

	}
	
	public NamespaceProperty getHw_watchdog_action() {
		return hw_watchdog_action;
	}
	
	public NamespaceProperty getHypervisor_type() {
		return hypervisor_type;
	}

}
