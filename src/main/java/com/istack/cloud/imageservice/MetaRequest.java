package com.istack.cloud.imageservice;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * MetaRequestHelper is a class for helping create post and put request
 * @author xvzezi
 */

public class MetaRequest {
	private String name;
	
	@JsonCreator
	public MetaRequest(@JsonProperty("name") String name)
	{
		this.name = name;
	}
	
	public String getName()
	{
		return this.name;
	}
}

