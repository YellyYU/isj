package com.istack.cloud.imageservice;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class UpdateContent {
	private String namespace;
	
	@JsonCreator
	public UpdateContent(@JsonProperty("namespace")String name_space){
		this.namespace=name_space;
	}
	
	public String getNamespace(){
		return namespace;
	}
}
