package com.istack.cloud.imageservice;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Input is a class used to create object for POST body in creating task.
 */
public class Input {
    private String import_from;
    private String import_from_format;
    private ImageProperties properties;
    @JsonCreator
    public Input(@JsonProperty("import_from") String import_from,
                 @JsonProperty("import_from_format") String import_from_format,
                 @JsonProperty("image_properties") ImageProperties properties)
    {
        this.import_from = import_from;
        this.import_from_format = import_from_format;
        this.properties = properties;
    }

    public ImageProperties getProperties() {
        return properties;
    }

    public String getImport_from() {
        return import_from;
    }

    public String getImport_from_format() {
        return import_from_format;
    }
}
