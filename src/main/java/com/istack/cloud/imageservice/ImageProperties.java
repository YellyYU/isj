package com.istack.cloud.imageservice;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * ImageProperties is a class used to create object for POST body in creating task.
 */
public class ImageProperties {
    private String disk_format;
    private String container_format;

    @JsonCreator
    public ImageProperties(@JsonProperty("disk_format") String disk_format,
                           @JsonProperty("container_format") String container_format)
    {
        this.disk_format = disk_format;
        this.container_format = container_format;
    }

    public String getContainer_format() {
        return container_format;
    }

    public String getDisk_format() {
        return disk_format;
    }
}
