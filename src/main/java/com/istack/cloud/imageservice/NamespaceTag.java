package com.istack.cloud.imageservice;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;


public class NamespaceTag {

	private String name; 
	
	@JsonCreator
	public NamespaceTag(
			@JsonProperty("name") String name)
	{
		this.name = name;
	}
	
	
	public String getName(){
		return name;
	}
	
}
