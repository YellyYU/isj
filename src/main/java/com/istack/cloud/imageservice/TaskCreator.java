package com.istack.cloud.imageservice;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * TaskCreator class is used to form the complete body for POST body in creating task
 */
public class TaskCreator {
    private String type;
    private Input  input;

    @JsonCreator
    public TaskCreator(@JsonProperty("type") String type, @JsonProperty("input") Input input)
    {
        this.type = type;
        this.input = input;
    }

    public Input getInput() {
        return input;
    }

    public String getType() {
        return type;
    }
}
