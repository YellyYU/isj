package com.istack.cloud.imageservice;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

public class MetadataDefinitionObject {
	
	private String namespace;
	private String objectName;
	private String urlHead;
	private String apiUrl;
	private RestTemplate httpConnection;
	private HttpEntity<?> request;
	
	/**
	 * A function help to form URL. 
	 */
	private void formUrl() {
		this.apiUrl = this.urlHead + "/v2/metadefs/namespaces/" + this.namespace + "/objects/" + this.objectName;
	}
	
	/**
	 * Similar to Identity, except apiUrl is not required, since the image service use 
	 * a different url. Hence, the user has to set the url and corresponding names  
	 * by calling setUrl. 
	 */
	public MetadataDefinitionObject (HttpEntity<?> request, RestTemplate restTemplate) {
		
		this.request = request;
		this.httpConnection = restTemplate;
	}
	
	/**
	 * @param urlHead 		Sets prefix of the url
	 * @param namespace 	Sets the fields namespace.
	 * @param objectName 	Set  the fileds objectName.
	 * The last two parameter can be modified by calling setNamespace(newName)
	 * and setObjectName(newName). They are need in url since it's in the 
	 * format /v2/metadefs/namespaces/​{namespace}/objects/{object_name}
	 */
	public void setUrl(String urlHead, String namespace, String objectName) {
		this.urlHead = urlHead; 
		setNamespace(namespace);
		setObjectName(objectName);
		formUrl();
	}
	
	/**
	 * Task: Get /v2/metadefs/namespaces/​{namespace}/objects/{object_name}  
	 * Shows the definition for an object.
	 * @return the response body if get succeeds, empty string otherwise.
	 */
	public String get() {
		try {
			return httpConnection.exchange(apiUrl, HttpMethod.GET, request, String.class).getBody();
		} catch (HttpClientErrorException error) {
			System.out.println("Http Error when get:" + error.getMessage());
			return "";
		}
	}
	
	/** 
	 * Task: Delete /v2/metadefs/namespaces/​{namespace}/objects/{object_name}
	 * Deletes an object definition from a namespace.
	 * No request body nor response body.
	 * @return true if the deletion succeeds, false otherwise.
	 */
	public boolean delete() {
		try {
			httpConnection.exchange(apiUrl, HttpMethod.DELETE, request, String.class);
		} catch (HttpClientErrorException error) {
			System.out.println("Http Error when delete:" + error.getMessage());
			return false;
		}
		return true;
	}
	
	/**
	 * Task: Put /v2/metadefs/namespaces/​{namespace}/objects/{object_name}
	 * Updates an object definition in a namespace.
	 * @param body is the request body.
	 * @return the response body if put succeeds, empty String otherwise.
	 */
	public String put(String body) {
		HttpHeaders headers = request.getHeaders();
		HttpEntity<String> entity = new HttpEntity<String>(body, headers);
		try {
			return httpConnection.exchange(apiUrl, HttpMethod.PUT, entity, String.class).getBody();
		} catch (HttpClientErrorException error) {
			System.out.println("Http Error when put:" + error.getMessage());
			return "";
		}
	}
	
	//	2 simple getters and 2 simple setters.
	public String getNamespace() {
		return namespace;
	}
	
	public void setNamespace(String namespace) {
		this.namespace = namespace;
		formUrl();
	}
	
	public String getObjectName() {
		return objectName;
	}
	
	public void setObjectName(String objectName) {
		this.objectName = objectName;
		formUrl();
	}
}

