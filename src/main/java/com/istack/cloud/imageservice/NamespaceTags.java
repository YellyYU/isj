package com.istack.cloud.imageservice;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.ArrayList;

public class NamespaceTags {
	private ArrayList<NamespaceTag> tags;
	
	@JsonCreator
	public NamespaceTags(
			@JsonProperty("tags") ArrayList<NamespaceTag> tags){
		
		this.tags = tags;
	}
	
	public ArrayList<NamespaceTag> getTags(){
		return tags;
	}
	
}
