package com.istack.cloud.imageservice;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class TestHelper21 {
		private String namespace;
		
		@JsonCreator
		public TestHelper21(@JsonProperty("namespace") String namespace)
		{
			this.namespace = namespace;
		}
		
		public String getNamespace()
		{
			return namespace;
		}
}
