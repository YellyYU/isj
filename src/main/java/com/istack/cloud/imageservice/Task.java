package com.istack.cloud.imageservice;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * This is the Task class.
 *
 * Created by kevin on 11/16/15.
 */
public class Task {
    private String id;
    private String type;
    private String status;

    @JsonCreator
    public Task(@JsonProperty("id") String id, @JsonProperty("type") String type,
                @JsonProperty("status") String status)
    {
        this.id = id;
        this.type = type;
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public String getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    public String toString() {
        return "id: " + this.id + "\n" +"type: " + this.type + "\n" +"status: " + this.status;
    }
}
