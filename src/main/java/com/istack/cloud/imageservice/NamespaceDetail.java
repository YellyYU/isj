package com.istack.cloud.imageservice;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class NamespaceDetail {
	String Description;
	String DisplayName;
	String Namespace;
	String Owner;
	Properties properties; 
	boolean Protected;
	Resource_type_associations[] resource_type_associations;
	String visibility;
	@JsonCreator
	NamespaceDetail(
			@JsonProperty("display_name")String display_name,
			@JsonProperty("description")String description,
			@JsonProperty("namespace")String namespace,
			@JsonProperty("visibility")String visibility,
			@JsonProperty("protected")boolean Protected,
			@JsonProperty("owner")String owner,
			@JsonProperty("resource_type_associations")Resource_type_associations[] rta,
			@JsonProperty("properties")Properties properties)
	{
		this.Description = description;
		this.DisplayName = display_name;
		this.Namespace = namespace;
		this.Owner = owner;
		this.properties = properties;
		this.Protected = Protected;
		this.resource_type_associations = rta;
		this.visibility = visibility;
	}
	//get and set functions
	public String getDescription(){
		return Description;
	}
	public String getDisplay_name(){
		return DisplayName;
	}
	public String getNamespace(){
		return Namespace;
	}
	public String getOwner(){
		return Owner;
	}
	public Properties getProperties(){
		return properties;
	}
	public boolean getProtected(){
		return Protected;
	}
	public String getVisibility(){
		return visibility;
	}
	public Resource_type_associations[] getResource_type_associations(){
		return resource_type_associations;
	}
	public String setDescription(String des){
		this.Description = des;
		return Description;
	}
	public String setDisplay_name(String dis){
		this.DisplayName = dis;
		return DisplayName;
	}
	public String setNamespace(String np){
		this.Namespace = np;
		return Namespace;
	}
	public String setOwner(String owner){
		this.Owner = owner;
		return Owner;
	}
	public Properties setProperties(Properties pro){
		this.properties = pro;
		return properties;
	}
	public boolean setProtected(boolean p){
		this.Protected = p;
		return Protected;
	}
	public String setVisibility(String vis){
		this.visibility = vis;
		return visibility;
	}
	public Resource_type_associations[] setResource_type_associations(Resource_type_associations[] rta){
		this.resource_type_associations = rta;
		return resource_type_associations;
	}
}
