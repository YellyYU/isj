package com.istack.cloud.imageservice;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.web.client.RestTemplate;
import java.util.ArrayList;
import com.istack.cloud.imageservice.NamespaceTag;
import com.istack.cloud.imageservice.NamespaceTags;

/**
 * The class ImageService provides a interface for restAPI in image service v2.0.
 *
 */
public class ImageService {
    private String apiUrl;
    private HttpHeaders header = null;
    private HttpEntity<?> request = null;
    private RestTemplate restTemplate = null;
    public MetadataDefinitionObject metadataDefinitionObject;

    public ImageService(String url, String token) {
        this.apiUrl = url;
        header = new HttpHeaders();
        header.set("X-Auth-Token", token);
        request = new HttpEntity(header);
        this.restTemplate = new RestTemplate();
		this.metadataDefinitionObject = new MetadataDefinitionObject(request, restTemplate);
    }
    
    public HttpEntity<?> getRequest(){
    	return this.request;
    }
    
    /**
     * Metadata definition properties:
     * POST		/v2/metadefs/namespaces/{namespace}/properties		Create property
     * @param namespace
     * @param name
     * @param title
     * @param type
     * @param enume(Optional)
     * @param description(Optional)
     * @return (temporarily) String
     */

    public String createProperty(String namespace, String name, String title, String type, String[] enume, String description){
		NamespaceProperty cp = new NamespaceProperty(name, title, type, enume, description);
        request = new HttpEntity<Object>(cp, header);
		return restTemplate.postForObject(apiUrl+"/v2/metadefs/namespaces/"+namespace+"/properties", 
				request, String.class);
	}
    
    /**
     * Metadata definition properties:
     * GET		/v2/metadefs/namespaces/{namespace}/properties		List properties
     * @param namespace
     * @return (temporarily) String
     */
    public PropertyReceiver listProperties(String namespace){
		PropertyReceiver pr = restTemplate.exchange(apiUrl+"/v2/metadefs/namespaces/"+namespace+"/properties",
				HttpMethod.GET, request, PropertyReceiver.class).getBody();
		System.out.println("here");
		return pr;
	}

    /**
     * Tasks: 
     * POST		/v2/tasks		Create tasks
     * @param url
     * @param conn
     */
    public void postTask(String type, String import_from,
                           String import_from_format, String disk_format,
                           String container_format)
    {
        ImageProperties properties = new ImageProperties(disk_format, container_format);
        Input           input = new Input(import_from, import_from_format, properties);
        TaskCreator     taskCreator = new TaskCreator(type, input);
        request = new HttpEntity<Object>(taskCreator, header);
        String location = restTemplate.postForObject(apiUrl + "/v2/tasks", request, String.class);
    }

    /**
     * Tasks: 
     * GET		/v2/tasks		List tasks
     * @return Task[]
     * A array contains all of the tasks currently.
     */
    public Task[] getAllTasks()
    {
        TaskReceiver received = restTemplate.exchange(apiUrl + "/v2/tasks",
                                HttpMethod.GET, request, TaskReceiver.class).getBody();
        return received.getTasks();
    }

    /**
     * Correspond to restAPI in openstack:
     * Tasks: 
     * GET		/v2/tasks/{task_id}		Show tasks
     * 
     * @param Id
     * @return Task
     *
     * return the Task according to the id provided.
     */
    public Task getTask(String Id)
    {
        Task task = restTemplate.exchange(apiUrl + "/v2/tasks/" + Id, HttpMethod.GET, request,
                Task.class).getBody();
        return task;
    }

    
    // Issue 21, not confirmed, merged by author
    /**
     * Metadata definition tags(since API v2.0)
     * POST		/v2/metadefs/namespaces/{namespace}/tags/{name}	Add tag definition
     * Description: Adds a tag to the list of namespace tag definitions.
     * 
     * @param namespace, name
     * @return MetaDefsTags
     */
    public MetaDefsTags postAddMetaTagDef(String namespace, String name)
    {
    	request = new HttpEntity<Object>(null, header);
    	MetaDefsTags mdt = restTemplate.postForObject(apiUrl + "/v2/metadefs/namespaces/" + namespace + "/tags/" + name,
    			request, MetaDefsTags.class);
    	return mdt;
    }

    /**
     * Metadata definition tags(since API v2.0)
     * GET		/v2/metadefs/namespaces/{namespace}/tags/{name}	Get tag definition
     * Description: Gets a definition for a tag
     * 
     * @param namespace, name
     * @return MetaDefsTags
     */
    public MetaDefsTags getMetaTagDef(String namespace, String name)
    {
    	request = new HttpEntity<Object>(null, header);
    	MetaDefsTags mdt = restTemplate.exchange(apiUrl + "/v2/metadefs/namespaces/" + namespace + "/tags/" + name,
    			HttpMethod.GET, request, MetaDefsTags.class).getBody();
    	return mdt;
    }

    public NamespaceDetail getNamespaceDetail(String name_space){
		String URL = apiUrl + "/v2/metadefs/namespaces/" + name_space;
		NamespaceDetail detail = restTemplate.exchange(URL,HttpMethod.GET, request, NamespaceDetail.class).getBody();
		//String res = restTemplate.exchange(URL,HttpMethod.GET, request, String.class).getBody();
		//System.out.print(res);
		//NamespaceReceiver npd = restTemplate.exchange(URL,HttpMethod.GET, request, NamespaceReceiver.class).getBody();
		return detail;
	}
    public void putNamespace(String name_space, NamespaceDetail npd){
    	String URL=apiUrl+"/v2/metadefs/namespaces/"+name_space;
    	npd.setNamespace(name_space);
		HttpEntity<?> Request=new HttpEntity(npd,request.getHeaders());
		restTemplate.exchange(URL,HttpMethod.PUT, Request, NamespaceDetail.class);
    }
    public void deleteNamespace(String name_space){
		String URL=apiUrl+"/v2/metadefs/namespaces/"+name_space;
		restTemplate.exchange(URL,HttpMethod.DELETE, request, String.class);
	}
    
    /**
     * Metadata definition tags(since API v2.0)
     * PUT		/v2/metadefs/namespaces/{namespace}/tags/{name}	Update tag definition
     * Description: Rename a tag definition.
     * 
     * @param namespace, name, newName
     * @return String
     */
    public String putUpdateMetaTagDef(String namespace, String name, String newName)
    {
    	MetaRequest mdtnd = new MetaRequest(newName);
    	request = new HttpEntity<Object>(mdtnd, header);
    	mdtnd = restTemplate.exchange(apiUrl + "/v2/metadefs/namespaces/" + namespace + "/tags/" + name,
    			HttpMethod.PUT, request, MetaRequest.class).getBody();
    	return mdtnd.getName();
    }

    /**
     * Metadata definition tags(since API v2.0)
     * DELETE		/v2/metadefs/namespaces/{namespaces}/tags/{name}	Delete tag definition
     * Description: Delete a tag definition within a namespace.
     * 
     * @param namespace, name
     * @return void
     */
    public void deleteMetaTagDef(String namespace, String name)
    {
    	request = new HttpEntity<Object>(null, header);
    	String loc = restTemplate.exchange(apiUrl + "/v2/metadefs/namespaces/" + namespace + "/tags/" + name,
    			HttpMethod.DELETE, request, String.class).getBody();
    }
    
    // Above: issue 21, not confirmed, merged by authorf

    
    
    public NamespaceTags getTags(String namespace)
    {
    	NamespaceTags nt = restTemplate.exchange(apiUrl+"/v2/metadefs/namespaces/"+namespace+"/tags",
				HttpMethod.GET, request, NamespaceTags.class).getBody();
		//System.out.println("here");
		return nt;
    }
 
    //public String deleteTags(String namespace)
    //{
	//	return restTemplate.exchange(apiUrl+"/v2/metadefs/namespaces/"+namespace+"/tags",
	//			HttpMethod.DELETE, request, String.class).getBody();
    //}
    
     public void deleteTags(String namespace)
     {
		restTemplate.exchange(apiUrl+"/v2/metadefs/namespaces/"+namespace+"/tags",
				HttpMethod.DELETE, request, String.class);
     }

     public NamespaceTags postTags(String namespace, ArrayList<NamespaceTag> names)
     {
	  
		NamespaceTags nt = new NamespaceTags(names);
        request = new HttpEntity<Object>(nt, header);
        //System.out.println("here");
		return restTemplate.postForObject(apiUrl+"/v2/metadefs/namespaces/"+namespace+"/tags", 
				request, NamespaceTags.class);
	 }

}