package com.istack.cloud.imageservice;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by kevin on 11/26/15.
 *
 * This class is used to receive and process the GET method of tasks
 */
public class TaskReceiver {
    private Task[] tasks;

    @JsonCreator
    public TaskReceiver(@JsonProperty("tasks") Task[] allTasks)
    {
        this.tasks = allTasks;
    }

    public Task[] getTasks()
    {
        return this.tasks;
    }
}
