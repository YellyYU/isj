package com.istack.cloud.imageservice;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Resource_type_associations {
	String CreatedAt;
	String Name;
	String Prefix;
	String Properties_target;
	String UpdatedAt;
	public String getCreated_at(){
		return CreatedAt;
	}
	public String getName(){
		return Name;
	}
	public String getPrefix(){
		return Prefix;
	}
	public String geyProperties_target(){
		return Properties_target;
	}
	@JsonCreator
	Resource_type_associations( @JsonProperty("created_at")String created_at,
								@JsonProperty("prefix")String prefix,
								@JsonProperty("properties_target")String properties_target,
								@JsonProperty("name")String name,
								@JsonProperty("updated_at")String updated_at)
	{
		this.CreatedAt = created_at;
		this.Name = name;
		this.Prefix = prefix;
		this.Properties_target = properties_target;
		this.UpdatedAt = updated_at;
	}
}
