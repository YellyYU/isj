package com.istack.cloud.imageservice;

import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * MetaDefsTags is a class for receiving object
 * @author xvzezi
 */

public class MetaDefsTags {
	private Date created_at;
	private String name;
	private Date updated_at;
	
	@JsonCreator
	public MetaDefsTags(@JsonProperty("created_at")String cra,
						   @JsonProperty("name")String na,
						   @JsonProperty("updated_at")String upa) throws ParseException
	{
		this.name = na;
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		this.created_at = sdf.parse(cra.substring(0, cra.indexOf('T')) + ' ' + cra.substring(cra.indexOf('T') + 1, cra.indexOf('Z')));
		this.updated_at = sdf.parse(cra.substring(0, upa.indexOf('T')) + ' ' + upa.substring(upa.indexOf('T') + 1, upa.indexOf('Z')));
	}
	
	
	
	public Date getCreatedDate()
	{
		return this.created_at;
	}
	
	public Date getUpdatedDate()
	{
		return this.updated_at;
	}
	
	public String getName()
	{
		return this.name;
	}
}
