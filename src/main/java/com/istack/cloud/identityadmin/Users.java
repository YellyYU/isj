package com.istack.cloud.identityadmin;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Users {
	private User[] userarray;
	
	@JsonCreator
    public Users(
    		@JsonProperty("users") User[] userarray
    		)
    {
        this.userarray = userarray;
    }
	
	public User[] getUserArray(){
		return userarray;
	}
	
}
