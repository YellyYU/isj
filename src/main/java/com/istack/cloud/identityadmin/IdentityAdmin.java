package com.istack.cloud.identityadmin;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import com.fasterxml.jackson.databind.SerializationFeature;
import com.istack.cloud.identityadmin.api.TenantsResource;
import com.istack.cloud.identityadmin.api.TokensResource;
import com.istack.cloud.identityadmin.api.UsersResource;

public class IdentityAdmin {
	
	private String apiURL;
	
	private final TokensResource TOKENS;
	private final TenantsResource TENANTS;
	private final UsersResource USERS;
	
	public IdentityAdmin(String apiURL) {
		this.apiURL = apiURL+"/v2.0";
		TOKENS	= new TokensResource(this.apiURL+"/tokens");
		TENANTS	= new TenantsResource(this.apiURL+"/tenants");
		USERS	= new UsersResource(this.apiURL+"/users");
	}
	
	public TokensResource 	tokens() 	{ return TOKENS;	}
	public TenantsResource	tenants() 	{ return TENANTS;	}
	public UsersResource	users()		{ return USERS;		}
	
//	public Users getUsers(){
//		
//		return restTemplate.exchange(apiURL+"/v2.0/users", HttpMethod.GET, request, Users.class).getBody();
//	}
	
//	public User getUserInfo(String userId) throws JsonParseException, JsonMappingException, IOException{
//		ObjectMapper objectMapper= new ObjectMapper();
//		//objectMapper.configure(SerializationFeature.WRAP_ROOT_VALUE, true);
//		String userinfo = restTemplate.exchange(apiURL+"/v2.0/users/"+userId, HttpMethod.GET, request,String.class).getBody();
//		System.out.println(userinfo);
//		User user = objectMapper.readValue(userinfo, User.class);
//		return user;
//	}

}
