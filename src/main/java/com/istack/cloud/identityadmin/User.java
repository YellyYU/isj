package com.istack.cloud.identityadmin;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class User {
	private String username;
	private String name;
	private boolean enabled;
	private String email;
	private String id;
	private String tenantId;
	private String password;
	
    @JsonCreator
    public User(
    		@JsonProperty("username") String username, 
    		@JsonProperty("name") String name,
    		@JsonProperty("enabled") boolean enabled, 
    		@JsonProperty("email") String email, 
    		@JsonProperty("id") String id,
    		@JsonProperty("tenantId") String tenantId,
    		@JsonProperty("password")String password)
    {
        this.username = username;
        this.name = name;
        this.enabled = enabled;
        this.email = email;
        this.id = id;
        this.tenantId = tenantId;
        this.password = password;
    }
    
    public String getUsername(){
    	return username;
    }
    
    public String getName(){
    	return name;
    }
    
    public Boolean getEnabled(){
    	return enabled;
    }
    
    public String getEmail(){
    	return email;
    }
    
    public String getId(){
    	return id;
    }
    
    public String getTenantId(){
    	return tenantId;
    }
    
    public String getPassword(){
    	return password;
    }
 
    public String toString(){
    	return 	"user:[username: "+username+
    			", name: " + name+
    			", enabled: "+enabled+
    			", email: "+email+
    			", id: "+id+
    			", tenantId: "+tenantId+
    			", password: "+password+
    			"]";
    }
}
