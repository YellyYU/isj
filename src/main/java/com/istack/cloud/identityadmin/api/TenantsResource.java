package com.istack.cloud.identityadmin.api;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;

import com.istack.cloud.base.OpenStackRequest;
import com.istack.cloud.base.TokenProvider;
import com.istack.cloud.identity.RoleListInfo;
import com.istack.cloud.identity.TenantInfo;
import com.istack.cloud.identity.TenantListInfo;
import com.istack.cloud.identity.common.Tenant;

public class TenantsResource {
	private String apiURL;
	
	public TenantsResource(String apiURL) {
		this.apiURL = apiURL;
	}
	
	public List list(){
		return new List();
	}
	
	public TenantDetail showByName(String name){
		String add = "?name="+name;
		return new TenantDetail(add);
	}
	
	public TenantDetail showById(String tenantId){
		String add = "/"+tenantId;
		return new TenantDetail(add);
	}
	
	public RoleList listRoles(String tenantId, String userId){
		return new RoleList(tenantId, userId);
	}
	
	public class List extends OpenStackRequest<Object, TenantListInfo> {
		public List() {
			super(apiURL, HttpMethod.GET, "", null, TenantListInfo.class);
		}
	}
	
	public class TenantDetail extends OpenStackRequest<Object, TenantInfo>{
		public TenantDetail(String UrlAdd){
			super(apiURL, HttpMethod.GET,UrlAdd, null, TenantInfo.class);
		}
	}
	
	public class RoleList extends OpenStackRequest<Object, RoleListInfo>{
		public RoleList(String tenantId, String userId){
			super(apiURL, HttpMethod.GET, "/"+tenantId+"/users/"+userId, null, RoleListInfo.class);
		}
	}
}
