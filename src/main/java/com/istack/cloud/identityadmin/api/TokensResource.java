package com.istack.cloud.identityadmin.api;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.web.client.RestTemplate;

import com.istack.cloud.base.*;
import com.istack.cloud.identity.Access;
import com.istack.cloud.identity.EndpointInfo;
import com.istack.cloud.identity.Login;

public class TokensResource {
	
	private String apiURL;
	
	public TokensResource(String apiURL) {
		this.apiURL = apiURL;
	}
	
	public Auth auth(Login login) {
		return new Auth(login);
	}
	
	public Validate validate(String tokenId) {
		return new Validate(tokenId);
	}
	
	public ValidateAdmin validate_admin(String tokenId){
		return new ValidateAdmin(tokenId);
	}
	
	public Delete delete(String tokenId){
		return new Delete(tokenId);
	}
	
	public ListEndpoints listEndpoints(String tokenId){
		return new ListEndpoints(tokenId);
	}
	
	public class Auth extends OpenStackRequest<Login, Access> {
		public Auth(Login login) {
			super(apiURL, HttpMethod.POST, "", login, Access.class);
		}
	}
	
	public class Validate extends OpenStackRequest<Object, Access> {
		public Validate(String tokenId) {
			super(apiURL, HttpMethod.GET, "/"+tokenId, null, Access.class);
		}
	}
	
	public class ValidateAdmin extends OpenStackRequest<Object, Object> {
		public ValidateAdmin(String tokenId) {
			super(apiURL, HttpMethod.HEAD, "/"+tokenId, null, Object.class);
		}
	}
	
	public class Delete extends OpenStackRequest<Object, Object> {
		public Delete(String tokenId) {
			super(apiURL, HttpMethod.DELETE, "/"+tokenId, null, Object.class);
		}
	}
	
	public class ListEndpoints extends OpenStackRequest<Object, EndpointInfo> {
		public ListEndpoints(String tokenId) {
			super(apiURL, HttpMethod.GET, "/"+tokenId+"/endpoints", null, EndpointInfo.class);
		}
	}
}
