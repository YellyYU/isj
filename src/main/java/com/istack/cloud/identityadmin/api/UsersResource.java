package com.istack.cloud.identityadmin.api;

import org.springframework.http.HttpMethod;

import com.istack.cloud.base.OpenStackRequest;
import com.istack.cloud.identity.common.RolesInfo;
import com.istack.cloud.identity.common.User;
import com.istack.cloud.identity.common.Users;

public class UsersResource {
	private String apiURL;
	
	public UsersResource(String apiURL) {
		this.apiURL = apiURL;
	}
	
	public Create create(User user){
		return new Create(user);
	}
	
	public Delete delete(String id){
		return new Delete(id);
	}
	
	public Show show(String id){
		return new Show(id);
	}
	
	public List list(){
		return new List();
	}
	
	public Update update(String id, User user) {
		user.getUserInfo().setId(id);
		return new Update(id, user);
	}
	
	public class Create extends OpenStackRequest<User,User>{
		public Create(User user){
			super(apiURL,HttpMethod.POST,"",user,User.class);
		}
	}
	
	public class Show extends OpenStackRequest<Void, User>{
		public Show(String id) {
			super(apiURL, HttpMethod.GET, new StringBuilder("/").append(id).toString(), null, User.class);
		}
	}
	
	public class Delete extends OpenStackRequest<Void, Void>{
		public Delete(String id) {
			super(apiURL, HttpMethod.DELETE, new StringBuilder("/").append(id).toString(), null, Void.class);
		}
	}
	
	public class List extends OpenStackRequest<Void, Users>{
		public List() {
			super(apiURL, HttpMethod.GET, "", null, Users.class);
		}
	}
	
	public class Update extends OpenStackRequest<User, User>{
		public Update(String id, User user) {
			super(apiURL, HttpMethod.PUT, new StringBuilder("/").append(id).toString(), user, User.class);
		}
	}
}
