package com.istack.cloud.identityadmin;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class UserInfo {
	private User user;
	
	@JsonCreator
	public UserInfo(
			@JsonProperty("user") User user
			)
	{
		this.user = user;
	}
	
	public User getUser(){
		return user;
	}
}
